-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2020 at 11:42 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `use_access` varchar(255) NOT NULL,
  `email_access` varchar(255) NOT NULL,
  `inet_access` varchar(255) NOT NULL,
  `printer_access` varchar(255) NOT NULL,
  `usb_access` varchar(255) NOT NULL,
  `file_sharing_access` varchar(255) NOT NULL,
  `domain_access` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `use_access`, `email_access`, `inet_access`, `printer_access`, `usb_access`, `file_sharing_access`, `domain_access`) VALUES
(1, 'Computer', 'coba', 'Yes', 'No', 'No', 'coba', 'coba');

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `asset_id` varchar(10) NOT NULL,
  `asset_type` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `tgl_masuk` varchar(100) NOT NULL,
  `user` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`asset_id`, `asset_type`, `asset_name`, `jumlah`, `tgl_masuk`, `user`, `status`, `id`) VALUES
('A00001', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', '06/18/2014', '', '', 2),
('A00002', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', '07/17/2014', '', '', 3),
('A00003', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', '08/18/2014', '', '', 4),
('A00004', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', '11/03/2015', '', '', 5),
('A00005', 'OS', 'Windows 7 Professional SP1 32 Bit', '2', '11/03/2015', '', '', 6),
('A00006', 'OS', 'Windows 8.1 Home Basic 64 bit', '1', '11/03/2015', '', '', 7),
('A00007', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '03/14/2014', '', '', 8),
('A00008', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 9),
('A00009', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 10),
('A00010', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 11),
('A00011', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 12),
('A00012', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 13),
('A00013', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 14),
('A00014', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 15),
('A00015', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '11/03/2015', '', '', 16),
('A00016', 'Computer', 'Komputer Rakitan', '1', '11/03/2015', '', '', 17),
('A00017', 'Computer', 'Komputer Rakitan', '1', '11/03/2015', '', '', 18),
('A00018', 'Computer', 'Komputer Rakitan', '1', '11/03/2015', '', '', 19),
('A00019', 'Computer', 'Komputer Rakitan', '1', '06/18/2014', '', '', 20),
('A00020', 'Printer', 'HP Laserjet 1025 CP', '1', '11/03/2015', '', '', 21),
('A00021', 'Microsoft', 'Office 2016 Home & Business 2016 FPP', '7', '11/03/2015', '', '', 22),
('A00022', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 23),
('A00023', 'Laptop', 'Lenovo Vostro 1200', '1', '11/03/2015', '', '', 24),
('A00024', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 25),
('A00025', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 26),
('A00026', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 27),
('A00027', 'Laptop', 'Fujitsu L H531', '1', '11/03/2015', '', '', 28),
('A00028', 'Computer', 'All In One Lenovo C240', '1', '11/03/2015', '', '', 29),
('A00029', 'Laptop', 'Lenovo G410', '1', '11/03/2015', '', '', 30),
('A00030', 'Laptop', 'HP 1000', '1', '11/01/2014', '', '', 31),
('A00031', 'Computer', 'All In One Lenovo C340', '1', '03/12/2014', '', '', 32),
('A00032', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 33),
('A00033', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 34),
('A00034', 'Computer', 'Komputer Rakitan', '1', '11/03/2015', '', '', 35),
('A00035', 'Laptop', 'Lenovo Vostro 1200', '1', '11/03/2015', '', '', 36),
('A00036', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 37),
('A00037', 'Computer', 'All In One Lenovo C240', '1', '11/03/2015', '', '', 38),
('A00038', 'Computer', 'All In One Lenovo C240', '1', '11/03/2015', '', '', 39),
('A00040', 'Laptop', 'HP 1431', '1', '11/03/2015', '', '', 41),
('A00041', 'Computer', 'Komputer Rakitan', '1', '08/18/2014', '', '', 42),
('A00042', 'Computer', 'All In One Lenovo C240', '1', '11/04/2015', '', '', 43),
('A00043', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 44),
('A00044', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 45),
('A00045', 'Computer', 'All In One Lenovo C240', '1', '11/03/2015', '', '', 46),
('A00046', 'Computer', 'HP Pavillion 20 All in One A2', '1', '03/12/2015', '', '', 47),
('A00047', 'Computer', 'All In One Lenovo C240', '1', '11/03/2015', '', '', 48),
('A00048', 'Laptop', 'Asus', '1', '11/03/2015', '', '', 49),
('A00049', 'Laptop', 'Asus', '1', '11/03/2015', '', '', 50),
('A00050', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 51),
('A00051', 'Computer', 'Komputer Rakitan', '1', '11/03/2015', '', '', 52),
('A00052', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 53),
('A00053', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '10/27/2015', '', '', 54),
('A00054', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', '11/03/2015', '', '', 55),
('A00055', 'Computer', 'All In One Lenovo C240', '1', '11/03/2014', '', '', 56),
('A00056', 'Printer', 'HP Laserjet 1025 CP', '1', '11/03/2015', '', '', 57),
('A00057', 'Printer', 'HP Laserjet 1025 CP', '1', '11/03/2015', '', '', 58),
('A00058', 'Printer', 'HP Laserjet 1020', '1', '11/03/2015', '', '', 59),
('A00059', 'Printer', 'DocuPrint M205b Fuji Xerox', '1', '11/03/2015', '', '', 60),
('A00060', 'Printer', 'Epson LX-310', '1', '10/02/2014', '', '', 61),
('A00061', 'Printer', 'Epson LX-310', '1', '02/12/2014', '', '', 62),
('A00062', 'Printer', 'Epson LX-310', '1', '11/03/2015', '', '', 63),
('A00063', 'Scanner', 'Canon Scan Lide 110', '1', '04/15/2014', '', '', 64),
('A00064', 'Printer', 'DocuPrint M205b Fuji Xerox', '1', '11/03/2015', '', '', 65),
('A00065', 'Server', 'Dell Power Edge 2950', '1', '11/03/2015', '', '', 66),
('A00066', 'Router', 'Cisco 1800 Series', '1', '11/03/2015', '', '', 67),
('A00067', 'Switch', 'Catalyst 2960S series SI 24 Port', '1', '11/03/2015', '', '', 68),
('A00068', 'UPS', 'APC Smart-UPS 5000', '1', '11/03/2015', '', '', 69),
('A00069', 'Modem', 'Linksys E 2500', '1', '11/03/2015', '', '', 70),
('A00070', 'Modem', 'Linksys Cisco  E1200', '1', '11/03/2015', '', '', 71),
('A00071', 'Modem', 'Linksys Cisco  E2500', '1', '11/04/2015', '', '', 72),
('A00072', 'Server', 'Dell Power Edge R210', '1', '11/03/2015', '', '', 73),
('A00073', 'CCTV', 'Calion 3024M', '1', '11/03/2015', '', '', 74),
('A00074', 'Switch', 'HP 1405 8 Port V2', '1', '11/03/2015', '', '', 75),
('A00075', 'Projector', 'Epson EB 925', '1', '11/03/2015', '', '', 76),
('A00076', 'Projector', 'Epson EB 925', '1', '11/03/2015', '', '', 77),
('A00077', 'Printer', 'Barcode Printer Toshiba BEX4T', '1', '01/02/2014', '', '', 78),
('A00078', 'Hardware', 'HDD External Toshiba 1 TB canvio Basic 3.0', '1', '02/12/2014', '', '', 79),
('A00079', 'USB', 'USB 8 GB Toshiba', '1', '02/12/2014', '', '', 80),
('A00080', 'Hardware', 'Mesin Ketik Elektrik Brother GX-6750', '1', '02/19/2014', '', '', 81),
('A00081', 'UPS', 'UPS Prolink Pro 700S 650 watt', '4', '03/03/2014', '', '', 82),
('A00085', 'Hardware', 'Connector Jack DC', '2', '03/12/2014', '', '', 86),
('A00094', 'Camera', 'Kamera Digital Cannon Ixus 145 ( 16MP)', '1', '08/08/2014', '', '', 95),
('A00095', 'Camera', 'Camera DIS 420', '1', '08/12/2014', '', '', 96),
('A00096', 'Laptop', 'Asus A450 LC', '1', '08/12/2014', '', '', 97),
('A00097', 'Camera', 'Kamera Digital Merk : Fuji JX 680 16 MP', '1', '08/14/2014', '', '', 98),
('A00098', 'Camera', 'Kamera Merk Fuji JV - 500 ( 14 MP ) Bonus : Memory', '1', '08/15/2014', '', '', 99),
('A00099', 'UPS', 'UPS Prolink Pro 700 V', '1', '08/18/2014', '', '', 100),
('A00100', 'Hardware', 'PSU 12 V 5 A and Box', '1', '09/16/2014', '', '', 101),
('A00101', 'CCTV', 'CCTV Calion', '5', '09/16/2014', '', '', 102),
('A00102', 'Switch', 'Switch hub hp Type : 1405, 8 part gigabyte', '100', '09/22/2014', '', '', 103),
('A00103', 'LCD', 'LCD Projector Epson EB X 200', '1', '10/20/2014', '', '', 104),
('A00104', 'Router', 'Router cisco linksys E 2500', '1', '11/06/2014', '', '', 105),
('A00105', 'Switch', 'Switch D - Link DÏƒS - 10008 A', '1', '11/12/2014', '', '', 106),
('A00106', 'UPS', 'UPS prolink pro 700scf', '1', '11/13/2014', '', '', 107),
('A00107', 'Mouse', 'Mouse usb logitech', '1', '02/09/2015', '', '', 108),
('A00108', 'Keyboard', 'Keyboard usb logitech k 120', '1', '02/09/2015', '', '', 109);

-- --------------------------------------------------------

--
-- Table structure for table `asset_pemakaian`
--

CREATE TABLE `asset_pemakaian` (
  `kode_asset` varchar(100) NOT NULL,
  `employee_id` varchar(100) NOT NULL,
  `asset_type` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `jumlah` varchar(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_keluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_pemakaian`
--

INSERT INTO `asset_pemakaian` (`kode_asset`, `employee_id`, `asset_type`, `asset_name`, `jumlah`, `keterangan`, `tgl_keluar`) VALUES
('1', 'Aniek Sulistyorini', 'Computer', 'All In One Lenovo C240', '1', '2', '11/10/2015'),
('2', 'Aniek Sulistyorini', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '2', '11/14/2015'),
('A82', 'Khoiri', 'Computer', 'Komputer Rakitan', '1', '1212', '11/30/2015');

-- --------------------------------------------------------

--
-- Table structure for table `coba`
--

CREATE TABLE `coba` (
  `id` int(11) NOT NULL,
  `no` varchar(255) NOT NULL,
  `asset_code` varchar(255) NOT NULL,
  `asset_name` varchar(255) NOT NULL,
  `asset_type` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `purchase_data` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `content_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`content_id`, `title`, `content`) VALUES
(2, 'Vision', '<pre><span style=\"font-size: large;\"><strong>Vision</strong></span></pre>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: large;\">&nbsp; Driven by its passion for continous improvement, the State College has to vigorously pursue distinction and proficieny in delivering its statutory functions to the Filipino people in the fields of education, business, agro-fishery, industrial, science and technology, through committed and competent human resource, guided by the beacon of innovation and productivity towards the heights of elevated status. </span><br /><br /></p>'),
(3, 'History', '<pre><span style=\"font-size: large;\">HISTORY &nbsp;</span> </pre>\r\n<p style=\"text-align: justify;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Carlos Hilado Memorial State College, formerly Paglaum State College, is a public educational institution that aims to provide higher technological, professional and vocational instruction and training in science, agriculture and industrial fields as well as short term or vocational courses. It was Batas Pambansa Bilang 477 which integrated these three institutions of learning: the Negros Occidental College of Arts and Trades (NOCAT) in the Municipality of Talisay, Bacolod City National Trade School (BCNTS) in Alijis, Bacolod City, and the Negros Occidental Provincial Community College (NOPCC) in Bacolod City, into a tertiary state educational institution to be called Paglaum State College. Approved in 1983, the College Charter was implemented effective January 1, 1984, with Mr. Sulpicio P. Cartera as its President. The administrative seat of the first state college in Negros Occidental is located at the Talisay Campus which was originally established as Negros Occidental School of Arts and Trades (NOSAT) under R.A. 848, authored and sponsored by Hon. Carlos Hilado. It occupies a five-hectare land donated by the provincial government under Provincial Board Resolution No. 1163. The renaming of the college to Carlos Hilado Memorial State College was effected by virtue of House Bill No. 7707 authored by then Congressman Jose Carlos V. Lacson of the 3rd Congressional District, Province of Negros Occidental, and which finally became a law on May 5, 1994</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">&nbsp;&nbsp;&nbsp; Talisay Campus. July 1, 1954 marked the formal opening of NOSAT with Mr. Francisco Apilado as its first Superintendent and Mr. Gil H. Tenefrancia as Principal. There were five (5) full time teachers, with an initial enrolment of eighty-nine (89) secondary and trade technical students. The shop courses were General Metal Works, Practical Electricity and Woodworking. The first classes were held temporarily at Talisay Elementary School while the shop buildings and classrooms were under construction. NOSAT was a recipient of FOA-PHILCUA aid in terms of technical books, equipment, tools and machinery. Alijis Campus. The Alijis Campus of the Carlos Hilado Memorial State College is situated in a 5-hectare lot located at Barangay Alijis, Bacolod City. The lot was a donation of the late Dr. Antonio Lizares. The school was formerly established as the Bacolod City National Trade School. The establishment of this trade technical institution is pursuant to R.A. 3886 in 1968, authored by the late Congressman Inocencio V. Ferrer of the second congressional district of the Province of Negros Occidental. Fortune Towne. The Fortune Towne Campus of the Carlos Hilado Memorial State College was originally situated in Negros Occidental High School (NOHS), Bacolod City on a lot owned by the Provincial Government under Provincial Board Resolution No. 91 series of 1970. The school was formerly established as the Negros Occidental Provincial Community College and formally opened on July 13, 1970 with the following course offerings: Bachelor of Arts, Technical Education and Bachelor of Commerce. The initial operation of the school started in July 13, 1970, with an initial enrolment of 209 students. Classes were first housed at the Negros Occidental High School while the first building was constructed. Then Governor Alfredo L. Montelibano spearheaded the first operation of the NOPCC along with the members of the Board of Trustees. In June 1995, the campus transferred to its new site in Fortune Towne, Bacolod City. Binalbagan Campus. On Nov. 24, 2000, the Negros Occidental School of Fisheries (NOSOF) in Binalbagan, Negros Occidental was integrated to the Carlos Hilado Memorial State College system as an external campus by virtue of Resolution No. 46 series of 2000.</p>'),
(4, 'Footer', '<p style=\"text-align:center\">CHMSC Online Learning Managenment System</p>\r\n\r\n<p style=\"text-align:center\">All Rights Reserved &reg;2013</p>\r\n'),
(5, 'Upcoming Events', '<pre>\r\nUP COMING EVENTS</pre>\r\n\r\n<p><strong>&gt;</strong> EXAM</p>\r\n\r\n<p><strong>&gt;</strong> INTERCAMPUS MEET</p>\r\n\r\n<p><strong>&gt;</strong> DEFENSE</p>\r\n\r\n<p><strong>&gt;</strong> ENROLLMENT</p>\r\n\r\n<p>&nbsp;</p>\r\n'),
(6, 'Title', '<p><span style=\"font-family:trebuchet ms,geneva\">CHMSC Online Learning Management System</span></p>\r\n'),
(7, 'News', '<pre>\r\n<span style=\"font-size:medium\"><em><strong>Recent News\r\n</strong></em></span></pre>\r\n\r\n<h2><span style=\"font-size:small\">Extension and Community Services</span></h2>\r\n\r\n<p style=\"text-align:justify\">This technology package was promoted by the College of Industrial Technology Unit is an index to offer Practical Skills and Livelihood Training Program particularly to the Ina ngTahanan of Tayabas, Barangay Zone 15, Talisay City, Negros Occidental</p>\r\n\r\n<p style=\"text-align:justify\">The respondent of this technology package were mostly &ldquo;ina&rdquo; or mothers in PurokTayabas. There were twenty mothers who responded to the call of training and enhancing their sewing skills. The beginners projects include an apron, elastics waist skirts, pillow-cover and t-shirt style top. Short sleeve blouses with buttonholes or contoured seaming are also some of the many projects introduced to the mothers. Based on the interview conducted after the culmination activity, the projects done contributed as a means of earning to the respondents.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; In support to the thrust of the government to improve the health status of neighboring barangays, the Faculty and Staff of CHMSC ECS Fortune Towne, Bacolod City, launched its Medical Mission in Patag, Silay City. It was conducted last March 2010, to address the health needs of the people. A medical consultation is given to the residents of SitioPatag to attend to their health related problems that may need medical treatment. Medical tablets for headache, flu, fever, antibiotics and others were availed by the residents.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;The BAYAN-ANIHAN is a Food Production Program with a battle cry of &ldquo;GOODBYE GUTOM&rdquo;, advocating its legacy &ldquo;Food on the Table for every Filipino Family&rdquo; through backyard gardening. NGO&rsquo;s, governmental organizations, private and public sectors, business sectors are the cooperating agencies that support and facilitate this project and Carlos Hilado Memorial State College (CHMSC) is one of the identified partner school. Being a member institution in advocating its thrust, the school through its Extension and Community Services had conducted capability training workshop along this program identifying two deputy coordinators and trainers last November 26,27 and 28, 2009, with the end in view of implementing the project all throughout the neighboring towns, provinces and regions to help address poverty in the country. Program beneficiaries were the selected families of GawadKalinga (GK) in Hope Village, Brgy. Cabatangan, Talisay City, with 120 families beneficiaries; GK FIAT Village in Silay City with 30 beneficiaries; Bonbon Dream Village brgy. E. Lopez, Silay City with 60 beneficiaries; and respectively Had. Teresita and Had. Carmen in Talisay City, Negros Occidental both with 60 member beneficiaries. This program was introduced to 30 household members with the end in view of alleviating the quality standards of their living.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">The extension &amp; Community Services of the College conducted a series of consultations and meetings with the different local government units to assess technology needs to determines potential products to be developed considering the abundance of raw materials in their respective areas and their product marketability. The project was released in November 2009 in six cities in the province of Negros Occidental, namely San Carlos, Sagay, Silay, Bago, Himamaylan and Sipalay and the Municipality of E. B Magalona</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The City of San Carlos focused on peanut and fish processing. Sagay and bago focused on meat processing, while Silay City on fish processing. The City of Himamaylan is on sardines, and in Sipalay focused on fish processing specially on their famous BARONGAY product. The municipality of E.B Magalona focused on bangus deboning.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The food technology instructors are tasked to provide the needed skills along with the TLDC Livelihood project that each City is embarking on while the local government units provide the training venue for the project and the training equipment and machine were provided by the Department of Science and Technology.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n'),
(8, 'Announcements', '<pre>\r\n<span style=\"font-size:medium\"><em><strong>Announcements</strong></em></span></pre>\r\n\r\n<p>Examination Period: October 9-11, 2013</p>\r\n\r\n<p>Semestrial Break: October 12- November 3, 2013</p>\r\n\r\n<p>FASKFJASKFAFASFMFAS</p>\r\n\r\n<p>GASGA</p>\r\n'),
(10, 'Calendar', '<pre style=\"text-align:center\">\r\n<span style=\"font-size:medium\"><strong>&nbsp;CALENDAR OF EVENT</strong></span></pre>\r\n\r\n<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"line-height:1.6em; margin-left:auto; margin-right:auto\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>First Semester &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>June 10, 2013 to October 11, 2013&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Semestral Break</p>\r\n			</td>\r\n			<td>\r\n			<p>Oct. 12, 2013 to November 3, 2013</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Second Semester</p>\r\n			</td>\r\n			<td>\r\n			<p>Nov. 5, 2013 to March 27, 2014</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Summer Break</p>\r\n			</td>\r\n			<td>\r\n			<p>March 27, 2014 to April 8, 2014</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Summer</p>\r\n			</td>\r\n			<td>\r\n			<p>April 8 , 2014 to May 24, 2014</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"line-height:1.6em; margin-left:auto; margin-right:auto\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"4\">\r\n			<p><strong>June 5, 2013 to October 11, 2013 &ndash; First Semester AY 2013-2014</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 4, 2013 &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Orientation with the Parents of the College&nbsp;Freshmen</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Service</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5</p>\r\n			</td>\r\n			<td>\r\n			<p>College Personnel General Assembly</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 6,7</p>\r\n			</td>\r\n			<td>\r\n			<p>In-Service Training (Departmental)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 10</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Classes</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Orientation with Students by College/Campus/Department</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 19,20,21</p>\r\n			</td>\r\n			<td>\r\n			<p>Branch/Campus Visit for Administrative / Academic/Accreditation/ Concerns</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\">\r\n			<p>June</p>\r\n			</td>\r\n			<td>\r\n			<p>Club Organizations (By Discipline/Programs)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Student Affiliation/Induction Programs</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July</p>\r\n			</td>\r\n			<td>\r\n			<p>Nutrition Month (Sponsor: Laboratory School)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July 11, 12</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August&nbsp; 8, 9</p>\r\n			</td>\r\n			<td>\r\n			<p>Midterm Examinations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 19</p>\r\n			</td>\r\n			<td>\r\n			<p>ArawngLahi</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 23</p>\r\n			</td>\r\n			<td>\r\n			<p>Submission of Grade Sheets for Midterm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August</p>\r\n			</td>\r\n			<td>\r\n			<p>Recognition Program (Dean&rsquo;s List)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 26</p>\r\n			</td>\r\n			<td>\r\n			<p>National Heroes Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 28, 29, 30</p>\r\n			</td>\r\n			<td>\r\n			<p>Sports and Cultural Meet</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>September 19,20</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 5</p>\r\n			</td>\r\n			<td>\r\n			<p>Teachers&rsquo; Day / World Teachers&rsquo; Day</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 10, 11</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examination</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 12</p>\r\n			</td>\r\n			<td>\r\n			<p>Semestral Break</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"margin-left:auto; margin-right:auto\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"4\">\r\n			<p><strong>Nov. 4, 2013 to March 27, 2014 &ndash; Second Semester AY 2013-2014</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>November 4 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Start of Classes</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>November 19, 20, 21, 22</p>\r\n			</td>\r\n			<td>\r\n			<p>Intercampus Sports and Cultural Fest/College Week</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 5, 6</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 19,20</p>\r\n			</td>\r\n			<td>\r\n			<p>Thanksgiving Celebrations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 21</p>\r\n			</td>\r\n			<td>\r\n			<p>Start of Christmas Vacation</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 25</p>\r\n			</td>\r\n			<td>\r\n			<p>Christmas Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 30</p>\r\n			</td>\r\n			<td>\r\n			<p>Rizal Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 6, 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>Classes Resume</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 9, 10</p>\r\n			</td>\r\n			<td>\r\n			<p>Midterm Examinations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 29</p>\r\n			</td>\r\n			<td>\r\n			<p>Submission of Grades Sheets for Midterm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>February 13, 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 6, 7</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examinations (Graduating)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 13, 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examinations (Non-Graduating)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 17, 18, 19, 20, 21</p>\r\n			</td>\r\n			<td>\r\n			<p>Recognition / Graduation Rites</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 27</p>\r\n			</td>\r\n			<td>\r\n			<p>Last Day of Service for Faculty</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5, 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Service for SY 2014-2015</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-left:auto; margin-right:auto\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"2\">\r\n			<p><strong>FLAG RAISING CEREMONY-TALISAY CAMPUS</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>MONTHS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>UNIT-IN-CHARGE</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June, Sept. and Dec. 2013, March 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>COE</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July and October 2013, Jan. 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>SAS</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August and November 2013, Feb. 2014</p>\r\n\r\n			<p>April and May 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>CIT</p>\r\n\r\n			<p>GASS</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n'),
(14, 'Campuses', '<pre>\r\n<span style=\"font-size:16px\"><strong>Campuses</strong></span></pre>\r\n\r\n<ul>\r\n	<li>Alijis Campus</li>\r\n	<li>Binalbagan Campus</li>\r\n	<li>Fortunetown Campus</li>\r\n	<li>Talisay Campus<br />\r\n	&nbsp;</li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `domain`
--

CREATE TABLE `domain` (
  `domain_id` varchar(10) NOT NULL,
  `domain_user` varchar(50) NOT NULL,
  `domain_pass` varchar(50) NOT NULL,
  `domain_ip` varchar(50) NOT NULL,
  `employee_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `domain`
--

INSERT INTO `domain` (`domain_id`, `domain_user`, `domain_pass`, `domain_ip`, `employee_id`) VALUES
('D104', 'pdindriyasari', 'Ann1s4@30', '192.168.0.28', 'Putu Debbie Indriyasari'),
('D1053', 'esumantri', '3ri@2015', '192.168.0.64', 'Eri Sumantri'),
('D111', 'tarwanto', '@T@rw@nT0', '192.168.0.48', 'Tarwanto'),
('D115', 'sutanta', 'T@nt@2014', '192.168.0.66', 'Sutanta'),
('D1173', 'adinasari', '4yu@2015', '192.168.0.31', 'Ayu Dinasari'),
('D1186', 'rkurniasari', 'R@t1h2015', '192.168.0.49', 'Ratih Kurniasari'),
('D1207', 'akholidin', '@hm4d@2015', '192.168.0.22', 'Ahmad Kholidin'),
('D1211', 'mudhalifah', 'mudh@2015', '192.168.0.40', 'Mudhalifah'),
('D1212', 'dtriansah', 'd1k1@15', 'Wifi', 'Diki Triansah'),
('D123', 'haryono', '@76PsmTe', '192.168.0.57', 'Nawang Raharto'),
('D1231', 'wyuniati', 'w1nd@2015', 'not yet', 'Winda Yuniati'),
('D1246', 'bintoro', 'b1ntor0@15', '192.168.0.50', 'Bintoro Satriawan'),
('D1251', 'irahmawati', '1k@2015', '192.168.0.59', 'Ika Rahmawati'),
('D1252', 'ehartoyo', '3k0@2015', '192.168.0.42', 'Eko Hartoyo'),
('D1263', 'frochim', 'fauz@2015', '192.168.0.72', 'Fauzan Rochim'),
('D1265', 'hsuparyadi', 'h4rry@15', 'Wifi', 'Harry Suparyadi'),
('D15', 'tpmontes', 'T@d30@15', '192.168.0.56', 'Tadeo P Montes'),
('D16', 'mcperez', 'Mar3c13l', 'Wifi', 'Mareciel C Perez'),
('D185', 'sofian', 'S0f1@n15', '192.168.0.71', 'Kukuh Sediyatomo'),
('D319', 'vanny', 'v@nny@128', 'Wifi', 'Ivanny Vina Aviany'),
('D44', 'asulistyorini', 'An13k@15', '192.168.0.39', 'Aniek Sulistyorini'),
('D520', 'saminingsih', 'Diaz2403', '192.168.0.30', 'Sulistianah Aminingsih'),
('D540', 'masahlil', 'M@bdUl@15', '192.168.0.63', 'Yuni Umiarsih'),
('D566', 'dkrisna', 'D1@H2015', '192.168.0.29', 'Diah Krisnawati'),
('D672', 'dkurnianto', '74FF2h1k', '192.168.0.62', 'Nuryadi'),
('D737', 'msahlil', 's4hlil@15', '192.168.0.42', 'M. Abdul Sahlil'),
('D82', 'khoiri', 'KH0!r1@15', '192.168.0.31', 'Khoiri'),
('D850', 'esulistiono', '$ul1st10no', '192.168.0.52', 'Edy Sulistyono'),
('D852', 'splestari', '431@12R', '192.168.0.22', 'Sahebah Puji Lestari'),
('D853', 'kumam', 'K241@pf', '192.168.0.21', 'M. Khoirul Umam'),
('D952', 'pprabowo', 'PuT$@2015', '192.168.0.60', 'Putut Prabowo'),
('D962', 'antok', 'Ant0K@15', '192.168.0.61', 'Wagiyanto'),
('D994', 'msumaryono', 'Sum@Ry0no', '192.168.0.50', 'M. Sumaryono'),
('Dxx', 'irosita', '@r01msTa', '192.168.0.20', 'Sahebah Puji Lestari'),
('E1164', 'sanam', 'sayvirgo@09', '192.168.0.37', 'M. Syariful Anam');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `email_id` varchar(10) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `email_user` varchar(50) NOT NULL,
  `email_pass` varchar(50) NOT NULL,
  `employee_name` varchar(100) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `email_address`, `email_user`, `email_pass`, `employee_name`, `id`) VALUES
('E104', 'pdinriyasari@pacific-traders.com', 'pdinriyasari', 'P@971mse', 'Putu Debbie Indriyasari', 1),
('E1053', 'esumantri@pacific-traders.com', 'esumantri', '3ri@2015', 'Eri Sumantri', 2),
('E111', 'tarwanto@pacific-traders.com', 'tarwanto', 'T@w@3s0g', 'Tarwanto', 3),
('E115', 'sutanta@pacific-traders.com', 'sutanta', 'chE5et-f', 'Sutanta', 4),
('E1164', 'sanam@pacific-traders.com', 'sanam', 'An@m2015', 'M. Syariful Anam', 5),
('E1173', 'adinasari@pacific-traders.com', 'adinasari', 'ayu@2015', 'Ayu Dinasari', 6),
('E1186', 'rkurniasari', 'rkurniasari', 'rat1h@15', 'Ratih Kurniasari', 7),
('E1207', 'akholidin@pacific-traders.com', 'akholidin', 'ahm@d@15', 'Ahmad Kholidin', 8),
('E1212', 'dtriansah@pacific-traders.com', 'dtriansah', 'dik1@2015', 'Diki Triansah', 9),
('E123', 'nraharto@pacific-traders.com', 'nraharto', 'S7Educuy', 'Nawang Raharto', 10),
('E1231', 'wyuniati@pacific-traders.com', 'wyuniati', 'wind@2015', 'Winda Yuniati', 11),
('E1246', 'bsatriawan@pacific-traders.com', 'bsatriawan', 'bintor0@15', 'Bintoro Satriawan', 12),
('E1252', 'ehartoyo@pacific-traders.com', 'ehartoyo', 'ek0@2015', 'Eko Hartoyo', 13),
('E1263', 'frochim@pacific-traders.com', 'frochim', 'fauz@n15', 'Fauzan Rochim', 14),
('E1265', 'hsuparyadi@pacific-traders.com', 'hsuparyadi', 'harry@15', 'Harry Suparyadi', 15),
('E15', 'tpmontes@pacific-traders.com', 'tpmontes', 'T@d30@15', 'Tadeo P Montes', 16),
('E16', 'mcperez@pacific-traders.com ', 'mcperez', 'Mar3c13l', 'Mareciel C Perez', 17),
('E319', 'ivaviani@pacific-traders.com', 'ivaviani', '!2#4AbCd', 'Ivanny Vina Aviany', 18),
('E42', 'linhidayah@pacific-traders.com', '(el)linhidayah', 'm2th$Wap', 'Lukman Nurhidayah', 19),
('E44', 'asulistyorini@pacific-traders.com', 'asulistyorini', 'aniek@15', 'Aniek Sulistyorini', 20),
('E440', 'dwi@pacific-traders.com', 'dwi', '69DWi@13', 'Dwi Kurnianto', 21),
('E520', 'saminingsih@pacific-traders.com', 'saminingsih', 'Diaz2403', 'Sulistianah Aminingsih', 22),
('E540', 'yumiarsih@pacific-traders.com', 'yumiarsih', 'Yun1@14', 'Yuni Umiarsih', 23),
('E566', 'dkirsnawati@pacific-traders.com', 'dkrisnawati', 'D1ah@14', 'Diah Krisnawati', 24),
('E737', 'masahlil@pacific-traders.com', 'masahlil', 's5espaF*', 'M. Abdul Sahlil', 25),
('E82', 'khoiri@pacific-traders.com', 'khoiri', 'casw5pAC', 'Khoiri', 26),
('E850', 'esulistiyono@pacific-traders.com', 'esulistiono', 'Duf8a+us', 'Edy Sulistyono', 27),
('E852', 'splestari@pacific-traders.com', 'splestari', 'B4ugaSPa', 'Sahebah Puji Lestari', 28),
('E853', 'kumam@pacific-traders.com', 'kumam', 'V6x!jUtr', 'M. Khoirul Umam', 29),
('E952', 'pprabowo@pacific-traders.com', 'pprabowo', '4wUbR@ku', 'Putut Prabowo', 30),
('E962', 'antok@pacific-traders.com ', 'antok', '!2#4AbCd', 'Wagiyanto', 31),
('E994', 'msumaryono@pacific-traders.com', 'msumaryono', 'xEste8!V', 'M. Sumaryono', 32),
('HRD', 'hrdpacificfurniture@pacific-traders.com', 'hrdpacificfurniture', 'ptpacific14', '', 33);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` varchar(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `status` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `pake` varchar(30) NOT NULL,
  `file_sharing` varchar(200) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fullname`, `department`, `position`, `birth_date`, `no_hp`, `gender`, `status`, `address`, `pake`, `file_sharing`, `id`) VALUES
('000016', 'Mareciel C Perez', '-', 'Operational Director', '06/15/1977', '081293465010', 'Female', '', 'Philippines', 'Laptop', 'All Dept', 2),
('000042', 'Lukman Nurhidayah', 'Divisi I', 'Manager', '08/14/1983', '', 'Male', '', 'JL. BRINGIN ASRI NO 139 R Ngaliyan Semarang', 'Computer', 'PPIC', 3),
('000044', 'Aniek Sulistyorini', 'HRD', 'Manager', '05/29/1972', '087825373688', 'Female', 'Married', 'Soreang Residance E2/6 Bandung', 'Laptop', 'HRD', 4),
('000073', 'Tri Mulyanto', 'PPIC', 'Supervisor Packing', '11/26/2015', '', 'Male', 'Married', 'Kaliwungu Semarang', 'Computer', 'None', 5),
('000082', 'Khoiri', 'Finance', 'Manager', '06/22/1978', '085641552080', 'Male', 'Single', 'Kedungsari RT 01/08 Semarang', 'Computer', 'Acounting, PPIC', 6),
('000104', 'Putu Debbie Indriyasari', 'Finance', 'Staff', '08/26/1978', '081326627177', 'Female', 'Married', 'Jl Padi IV B No.172 Semarang', 'Computer', 'Acounting , PPIC', 7),
('000111', 'Tarwanto', 'PPIC', 'Manager', '09/10/1980', '08587651866', 'Male', 'Married', 'Semarang', 'Computer', 'PPIC', 8),
('000115', 'Sutanta', 'Maintenance', 'Supervisor', '12/25/1978', '', 'Male', '', 'Jl Sriwibowo Rt. 03/06 Semarang', 'Computer', 'Maintenance', 9),
('000123', 'Nawang Raharto', 'PDE', 'Supervisor', '12/11/1977', '087731274408', 'Male', '', 'Dk. Talun Kacanf RT.04/III Kandri Gunung Pati Semarang', 'Computer', 'PDE', 10),
('000185', 'Kukuh Sediyatomo', 'Veneering', 'Leadman', '05/10/1978', '085290120139', 'Male', 'Married', 'Jl. Ronggowarsito AST Depo Indah 7A Semarang Timur', 'Computer', 'PPIC', 11),
('000319', 'Ivanny Vina Aviany', 'Purchasing', 'Staff', '01/23/1994', '087700108808', 'Female', '', 'Jl. Soekarno Hatta No.119A Semarang', 'Computer', 'Acounting, Maintenance, Purchasing', 12),
('000440', 'Dwi Kurnianto', 'Costing', 'Supervisor', '09/11/1976', '081325113869', 'Male', '', 'Jl.Brotojoyo Barat III / No.12 Semarang', 'Laptop', 'PDE Costing', 13),
('000520', 'Sulistianah Aminingsih', 'Finance', 'Staff', '08/26/1978', '081326307088', 'Female', '', 'Jl. Abu Bakar No.52 RT.15/12 Tambak Aji', 'Computer', 'Accounting', 14),
('000540', 'Yuni Umiarsih', 'Warehouse', 'Admin', '07/18/1980', '085740106048', 'Female', 'Married', 'Srikaton Utara IV Semarang', 'Computer', 'Warehouse', 15),
('000566', 'Diah Krisnawati', 'CSG', 'Supervisor', '06/24/1992', '082134718146', 'Female', 'Married', 'Genuk Sari Semarang', 'Computer', 'Secretary, PPIC', 16),
('000672', 'Nuryadi', 'PDE', 'Enginer', '05/16/1980', '085226083096', 'Male', '', 'Jl. Siwarak RW.02 Gunung Pati Semarang', 'Computer', 'PDE', 17),
('000737', 'M. Abdul Sahlil', 'HRD', 'Staff', '03/15/1995', '08988856203', 'Male', 'Single', 'Jl. Arimbi 1 Desa Galeh Purwodadi', 'Computer', 'HRD', 18),
('000819', 'Yuliharika Wati', 'Finishing', 'Admin', '07/21/1988', '081221999290', 'Male', 'Married', 'Semarang', 'Computer', 'PPIC', 19),
('000850', 'Edy Sulistyono', 'Finishing', 'Manager', '07/09/1980', '081215805522', 'Male', 'Married', 'Kaliwungu BIK B66 Kendal', 'Laptop', 'PPIC', 20),
('000852', 'Sahebah Puji Lestari', 'Purchasing', 'Staff', '07/04/1996', '', 'Female', '', 'Semarang', 'Computer', 'Purchasing', 21),
('000853', 'M. Khoirul Umam', 'Purchasing', 'Supervisor', '12/16/1994', '087747927822', 'Male', 'Single', 'Kendal', 'Computer', 'Purchasing, Secretary', 22),
('000952', 'Putut Prabowo', 'PDE', 'Staff', '03/27/1980', '08156595835', 'Male', '', 'Semarang', 'Computer', 'PDE', 23),
('000962', 'Wagiyanto', 'PDE', 'Drafter', '01/29/1979', '', 'Male', '', 'Jl. Kaliwiru II 509A Semarang', 'Computer', 'PDE', 24),
('000994', 'M. Sumaryono', 'PPIC', 'Staff', '08/23/1981', '085740683134', 'Male', 'Married', 'Gubug', 'Computer', 'PPIC', 25),
('001053', 'Eri Sumantri', 'Warehouse', 'Admin', '01/15/1992', '08999108676', 'Male', 'Single', 'Jl. Sendangguwo Raya Semarang', 'Computer', 'Warehouse', 26),
('001164', 'M. Syariful Anam', 'IT', 'IT', '09/07/1992', '087898118877', 'Male', 'Single', 'Perum Bukit Jatisari Lestari A22/5 RT.02/11 Mijen Semarang', 'Laptop', 'IT', 27),
('001173', 'Ayu Dinasari', 'Finance', 'Staff', '04/16/1992', '085641646920', 'Female', 'Married', 'Jl. Tugu Lapangan RT 04/1 Semarang', 'Computer', 'Acounting', 28),
('001186', 'Ratih Kurniasari', 'Maintenance', 'Admin', '07/22/1991', '089668200238', 'Female', 'Married', 'Mijen RT.03 RW II Semarang', 'Computer', 'Maintenance', 29),
('001205', 'Sudarmanto', 'Exim', 'Staff', '06/30/1987', '081329686351', 'Male', 'Single', 'Kendal', 'Computer', 'Exim', 30),
('001207', 'Ahmad Kholidin', 'Purchasing', 'Staff', '05/15/1994', '089668058604', 'Male', 'Single', 'Gubugsari 3/5 Pegandon Kendal', 'Computer', 'purchasing', 31),
('001211', 'Mudhalifah', 'HRD', 'Staff', '06/12/1992', '085741525724', 'Female', 'Single', 'Mangkang Wetan RT 03/02 Tugu Semarang', 'Computer', 'HRD', 32),
('001212', 'Diki Triansah', 'Outsourching', 'Staff', '12/01/1997', '083861745670', 'Male', 'Single', 'Putat Gede 02/06 Ngampel Kendal', 'Computer', 'PPIC', 33),
('001231', 'Winda Yuniati', 'Finance', 'Admin', '11/10/1994', '085879337078', 'Female', 'Single', 'Jl. Genuk Krajan 5 No 19 RT 06/03 Kel Tegalsari Kec Candisan Semarang', 'Computer', 'Acounting', 34),
('001246', 'Bintoro Satriawan', 'PPIC', 'Staff', '05/04/1987', '085641781160', 'Male', '', 'Semarang', 'Computer', 'PPIC, Acounting ( Debby)', 35),
('001251', 'Ika Rahmawati', 'Costing', 'Admin', '09/21/1994', '081391611041', 'Female', '', 'Tegorejo RT 1/8 Pegandon Kendal Semarang', 'Computer', 'PDE Costing', 36),
('001252', 'Eko Hartoyo', 'HRD', 'Supervisor', '07/18/1976', '08567794819', 'Male', '', 'Pondok Bukit Agung F-11 Semarang', 'Computer', 'HRD', 37),
('001263', 'Fauzan Rochim', 'Exim', 'Admin', '09/06/1992', '087832301254', 'Male', '', 'Jl. Dewi Sartuji Barat Semarang', 'Computer', 'CSG / Exim', 38),
('001265', 'Harry Suparyadi', 'HRD', 'Manager', '03/17/1977', '08122923096', 'Male', 'Married', 'Gedenganak Ungaran', 'Laptop', 'HRD', 39),
('000333', 'Dimas Okva ', 'IT', 'IT', '2020-10-10', '000000000', 'Male', 'Single', 'Semarang', 'Computer', 'IT', 44),
('okva', 'okva', 'okva', 'okva', '2020-10-15', 'okva', 'Female', 'Single', 'okva', 'Laptop', 'okva', 45),
('wadaw', 'daw', 'daw', 'dawd', '2020-10-15', 'dawdwa', 'Female', 'Single', 'dwadaw', 'Computer', 'awdaw', 46);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(100) NOT NULL,
  `teacher_class_id` int(11) NOT NULL,
  `date_start` varchar(100) NOT NULL,
  `date_end` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `teacher_class_id`, `date_start`, `date_end`) VALUES
(15, 'Long Test', 113, '12/05/2013', '12/06/2013'),
(17, 'sdasf', 147, '11/16/2013', '11/16/2013');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`) VALUES
(15, 'admin', 'admin', 'admin', 'admin'),
(16, 'anam', 'anam', 'Syariful', 'Anam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_pemakaian`
--
ALTER TABLE `asset_pemakaian`
  ADD PRIMARY KEY (`kode_asset`);

--
-- Indexes for table `coba`
--
ALTER TABLE `coba`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `domain`
--
ALTER TABLE `domain`
  ADD PRIMARY KEY (`domain_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `coba`
--
ALTER TABLE `coba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
