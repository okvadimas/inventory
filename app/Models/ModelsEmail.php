<?php namespace App\Models;

use CodeIgniter\Model;

class ModelsEmail extends Model
{
    protected $table      = 'email';
    protected $primaryKey = 'id';
    protected $allowedFields = ['email_id', 'email_address', 'email_user', 'email_pass', 'employee_name'];

    public function search($keyword)
    {
        return $this->table("email")->like("email_id", $keyword)->orLike("email_address", $keyword)->orLike("email_user", $keyword)->orLike("email_pass", $keyword)->orLike("employee_name", $keyword);
    }
}