<?php namespace App\Models;

use CodeIgniter\Model;

class ModelsEmployee extends Model
{
    protected $table      = 'employee';
    protected $primaryKey = 'id';
    protected $allowedFields = ['employee_id', 'fullname', 'department', 'position', 'birth_date', 'no_hp', 'gender', 'status', 'address', 'pake', 'file_sharing'];

    public function search($keyword)
    {
        return $this->table("employee")->like("employee_id", $keyword)->orLike("fullname", $keyword)->orLike("department", $keyword)->orLike("position", $keyword)->orLike("birth_date", $keyword)->orLike("no_hp", $keyword)->orLike("gender", $keyword)->orLike("status", $keyword)->orLike("address", $keyword)->orLike("pake", $keyword)->orLike("file_sharing", $keyword);
    }
}