<?php namespace App\Models;

use CodeIgniter\Model;

class Models extends Model
{
    protected $table      = 'asset';
    protected $primaryKey = 'id';
    protected $allowedFields = ['asset_id', 'asset_name', 'asset_type', 'jumlah', 'tgl_masuk', 'user', 'status'];

    public function search($keyword)
    {
        return $this->table("asset")->like("asset_id", $keyword)->orLike("asset_type", $keyword)->orLike("asset_name", $keyword)->orLike("user", $keyword);
    }
}
