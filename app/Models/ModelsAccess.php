<?php namespace App\Models;

use CodeIgniter\Model;

class ModelsAccess extends Model
{
    protected $table      = 'access';
    protected $primaryKey = 'id';
    protected $allowedFields = ['use_access', 'email_access', 'inet_access', 'printer_access', 'usb_access', 'file_sharing_access', 'domain_access', 'domain_pass', 'anydesk'];

    public function search($keyword)
    {
        return $this->table("access")->like("use_access", $keyword)->orLike("email_access", $keyword)->orLike("file_sharing_access", $keyword)->orLike("domain_access", $keyword)->orLike("anydesk", $keyword);
    }
}
