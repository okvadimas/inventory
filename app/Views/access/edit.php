<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/accessfunction/update/<?= $items["id"]; ?>" method="post">
                        <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" class="form-control <?= ($validation->hasError("use_access")) ? 'is-invalid' : ''; ?>" name="use_access" id="use_access" placeholder="Input Here..." value="<?= (old("use_access")) ? old("use_access") : $items["use_access"]; ?>">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="domain_access">Domain Access</label>
                            <input type="text" class="form-control <?= ($validation->hasError("domain_access")) ? 'is-invalid' : ''; ?>" name="domain_access" id="domain_access" placeholder="Input Domain Access Here..." value="<?= (old("domain_access")) ? old("domain_access") : $items["domain_access"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("domain_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="domain_pass">Domain Password</label>
                            <input type="text" class="form-control <?= ($validation->hasError("domain_pass")) ? 'is-invalid' : ''; ?>" name="domain_pass" id="domain_pass" placeholder="Input Email Access Here..." value="<?= (old("domain_pass")) ? old("domain_pass") : $items["domain_pass"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("domain_pass"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="email_access">Email Access</label>
                            <input type="text" class="form-control <?= ($validation->hasError("email_access")) ? 'is-invalid' : ''; ?>" name="email_access" id="email_access" placeholder="Input Domain Access Here..." value="<?= (old("email_access")) ? old("email_access") : $items["email_access"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="anydesk">Anydesk ID</label>
                            <input type="text" class="form-control <?= ($validation->hasError("anydesk")) ? 'is-invalid' : ''; ?>" name="anydesk" id="anydesk" placeholder="Input Email Access Here..." value="<?= (old("anydesk")) ? old("anydesk") : $items["anydesk"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("anydesk"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inet_access">Internet Access</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inet_access" id="inlineRadio1" value="Yes" <?= ($items["inet_access"]==="Yes") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inet_access" id="inlineRadio2" value="No" <?= ($items["inet_access"]==="No") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                            <div class="invalid-feedback">
                                <?= $validation->getError("inet_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="printer_access">Printer Access</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="printer_access" id="inlineRadio3" value="Yes" <?= ($items["printer_access"]==="Yes") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio3">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="printer_access" id="inlineRadio4" value="No" <?= ($items["printer_access"]==="No") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio4">No</label>
                            </div>                            
                            <div class="invalid-feedback">
                                <?= $validation->getError("printer_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="usb_access">USB Access</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="usb_access" id="inlineRadio5" value="Yes" <?= ($items["usb_access"]==="Yes") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio5">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="usb_access" id="inlineRadio6" value="No" <?= ($items["usb_access"]==="No") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="inlineRadio6">No</label>
                            </div>                            
                            <div class="invalid-feedback">
                                <?= $validation->getError("usb_access"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="use_access">Use Access</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="use_access" id="pake1" value="Mobile Device" <?= ($items["use_access"]==="Mobile Device") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="pake1">Mobile Device</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="use_access" id="pake2" value="Laptop" <?= ($items["use_access"]==="Laptop") ? "checked" : ''; ?>>
                                <label class="form-check-label" for="pake2">Laptop</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="use_access" id="pake3" value="Computer" <?= ($items["use_access"]==='Computer')?'checked':'' ?>>
                                <label class="form-check-label" for="pake3">Computer</label>
                            </div>                            
                            <div class="invalid-feedback">
                                <?= $validation->getError("use_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="file_sharing_access">File Sharing Access</label>
                            <input type="text" class="form-control <?= ($validation->hasError("file_sharing_access")) ? 'is-invalid' : ''; ?>" name="file_sharing_access" id="file_sharing_access" placeholder="Input File Sharing Here..." value="<?= (old("file_sharing_access")) ? old("file_sharing_access") : $items["file_sharing_access"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("file_sharing_access"); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="update" class="btn btn-primary">Update Data</button>
                    <button type="submit" name="cancel" class="btn btn-danger"><a href="/access/" class="text-white text-decoration-none">Cancel</a></button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>