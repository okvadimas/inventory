<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<div class="container-fluid pt-4" style="background-color: white;   ">
    <div class=" title pl-4 pt-2 pb-2">
        <div class="body" style="position: absolute;">
            <h5 class="card-title font-weight-bold">Access Data Overview</h5>
            <h6 class="card-subtitle">Status inventory access </h6>
        </div>
        <div class="signout text-right pr-4">
            <a class="btn btn-danger fa fa-sign-out text-right" style="line-height: 25px;" href="/signin/logout"> Sign Out</a>
        </div>
    </div>
    <?php if (session()->getFlashData("pesan")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashData("pesan_gagal")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan_gagal") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="/access/" method="post">
        <div class="input-group pt-3 pb-3 pl-4 pr-4">
            <input type="text" class="form-control btn-outline-white" placeholder="Searching Item Here..." name="keyword" aria-describedby="button-addon2" value="<?= ($keyword) ? $keyword : ''; ?>">
            <div class="input-group-append">
                <button class="btn btn-outline-dark" type="submit" name="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="table-responsive pr-4 pl-4 pt-2">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Use</th>
                    <th scope="col">Domain</th>
                    <th scope="col">Domain Password</th>
                    <th scope="col">Email</th>
                    <th scope="col">Anydesk</th>
                    <th scope="col">Internet</th>
                    <th scope="col">Printer</th>
                    <th scope="col">USB</th>
                    <th scope="col">File Sharing</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <?php if ($null === true) : ?>
                <td colspan="11" class="text-center">Data Not Found!</td>
            <?php endif; ?>
            <tbody>
                <?php $angka = 1 + (20 * ($current_page - 1)); ?>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $angka++; ?></td>
                        <td><?= $item["use_access"]; ?></td>
                        <td><?= $item["domain_access"]; ?></td>
                        <td><?= $item["domain_pass"]; ?></td>
                        <td><?= $item["email_access"]; ?></td>
                        <td><?= $item["anydesk"]; ?></td>
                        <td><?= $item["inet_access"]; ?></td>
                        <td><?= $item["printer_access"]; ?></td>
                        <td><?= $item["usb_access"]; ?></td>
                        <td><?= $item["file_sharing_access"]; ?></td>
                        <td class="text-center">
                            <a href="/accessfunction/edit/<?= $item["id"]; ?>" class="fa fa-pencil fa-2x pr-3 text-dark"></a>
                            <a href="/accessfunction/delete/<?= $item["id"]; ?>" class="fa fa-trash fa-2x text-dark" data-toggle="modal" data-target="#modaldelete"></a>
                        </td>
                        <!-- MODALS DELETE -->
                        <div class="modal" id="modaldelete" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirmation !</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to delete this item ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-primary" onclick="document.location.href = '/accessfunction/delete/<?= $item['id']; ?>'">Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODALS DELETE -->
                        <!-- MODALS INSERT TEMPLATE -->
                        <div class="modal fade" id="modalinsert" tabindex="-1" aria-labelledby="modalinsert" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= $title_insert; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/accessfunction/save/<?= $item["id"]; ?>" method="post">
                                            <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                            <input type="hidden" class="form-control <?= ($validation->hasError("use_access")) ? 'is-invalid' : ''; ?>" name="use_access" id="use_access" placeholder="Input Here..." value="<?= (old("use_access")) ? old("use_access") : ''; ?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="domain_access">Domain Access</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("domain_access")) ? 'is-invalid' : ''; ?>" name="domain_access" id="domain_access" placeholder="Input Domain Access Here..." value="<?= (old("domain_access")) ? old("domain_access") : ''; ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("domain_access"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="domain_pass">Domain Password</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("domain_pass")) ? 'is-invalid' : ''; ?>" name="domain_pass" id="domain_pass" placeholder="Input Email Access Here..." value="<?= (old("domain_pass")) ? old("domain_pass") : ''; ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("domain_pass"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="email_access">Email Access</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("email_access")) ? 'is-invalid' : ''; ?>" name="email_access" id="email_access" placeholder="Input Domain Access Here..." value="<?= (old("email_access")) ? old("email_access") : ''; ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("email_access"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="anydesk">Anydesk ID</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("anydesk")) ? 'is-invalid' : ''; ?>" name="anydesk" id="anydesk" placeholder="Input Email Access Here..." value="<?= (old("anydesk")) ? old("anydesk") : ''; ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("anydesk"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inet_access">Internet Access</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inet_access" id="inlineRadio1" value="Yes">
                                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inet_access" id="inlineRadio2" value="No">
                                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("inet_access"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="printer_access">Printer Access</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="printer_access" id="inlineRadio3" value="Yes">
                                                        <label class="form-check-label" for="inlineRadio3">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="printer_access" id="inlineRadio4" value="No">
                                                        <label class="form-check-label" for="inlineRadio4">No</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("printer_access"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="usb_access">USB Access</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="usb_access" id="inlineRadio5" value="Yes">
                                                        <label class="form-check-label" for="inlineRadio5">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="usb_access" id="inlineRadio6" value="No">
                                                        <label class="form-check-label" for="inlineRadio6">No</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("usb_access"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="use_access">Use Access</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="use_access" id="pake1" value="Mobile Device">
                                                        <label class="form-check-label" for="pake1">Mobile Device</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="use_access" id="pake2" value="Laptop">
                                                        <label class="form-check-label" for="pake2">Laptop</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="use_access" id="pake3" value="Computer">
                                                        <label class="form-check-label" for="pake3">Computer</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("use_access"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="file_sharing_access">File Sharing Access</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("file_sharing_access")) ? 'is-invalid' : ''; ?>" name="file_sharing_access" id="file_sharing_access" placeholder="Input File Sharing Here..." value="<?= (old("file_sharing_access")) ? old("file_sharing_access") : ''; ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("file_sharing_access"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right justify-content-right mt-4">
                                                <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                                                <button type="submit" name="cancel" class="btn btn-danger"><a href="/access/" class="text-white text-decoration-none">Cancel</a></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals Insert Templates -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="pagination justify-content-left pb-2">
            <?= $pager->links("access", "item_pagination"); ?>
        </div>
    </div>

</div>

<!-- MODALS TEMPLATE -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="inputAddress">Asset Code</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2">Asset Name</label>
                        <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Asset Type</label>
                            <input type="email" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">User</label>
                            <input type="text" class="form-control" id="inputPassword4">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Divisi</label>
                            <input type="email" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location</label>
                            <input type="text" class="form-control" id="inputPassword4">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>

<?php $this->endSection(); ?>