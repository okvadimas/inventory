<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/assetfunction/save/<?= $items["id"]; ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here..." value="<?= old("asset_type"); ?>">                 
                    <div class="form-group">
                        <label for="asset_type">Asset Type</label>
                        <input type="text" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here..." value="<?= old("asset_type"); ?>">
                        <div class="invalid-feedback"> 
                            <?= $validation->getError("asset_type"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="asset_name">Asset Name</label>
                        <input type="text" class="form-control <?= ($validation->hasError("asset_name")) ? 'is-invalid' : ''; ?>" name="asset_name" id="asset_name" placeholder="Input Asset Name Here..." value="<?= old("asset_name"); ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("asset_name"); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="jumlah">Jumlah</label>
                            <input type="text" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="jumlah" id="jumlah" placeholder="Input Jumlah Here..." value="<?= old("jumlah"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("jumlah"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="user">User</label>
                            <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="user" id="user" placeholder="Input User Here..." value="<?= old("user"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("user"); ?> 
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="tgl_masuk">Tanggal Masuk</label>
                            <input type="date" class="form-control <?= ($validation->hasError("tgl_masuk")) ? 'is-invalid' : ''; ?>" name="tgl_masuk" id="tgl_masuk" placeholder="Tanggal Masuk..." value="<?= old("tgl_masuk"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("tgl_masuk"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="status">Status</label>
                            <input type="text" class="form-control <?= ($validation->hasError("status")) ? 'is-invalid' : ''; ?>" name="status" id="status" placeholder="Input Status Here..." value="<?= old("status"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("status"); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                    <button type="submit" name="cancel" class="btn btn-danger"><a href="/asset/" class="text-white text-decoration-none">Cancel</a></button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>