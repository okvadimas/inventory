<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<div class="container-fluid pt-4" style="background-color: white;">
    <div class=" title pl-4 pt-2 pb-2">
        <div class="body" style="position: absolute;">
            <h5 class="card-title font-weight-bold">Asset Data Overview</h5>
            <h6 class="card-subtitle">Status inventory asset </h6>
        </div>
        <div class="signout text-right pr-4">
            <a class="btn btn-danger fa fa-sign-out text-right" style="line-height: 25px;" href="/signin/logout"> Sign Out</a>
        </div>
    </div>
    <?php if (session()->getFlashData("pesan")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashData("pesan_gagal")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan_gagal") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="/asset/" method="post">
        <div class="input-group pt-3 pb-3 pl-4 pr-4">
            <input type="text" class="form-control btn-outline-white" placeholder="Searching Item Here..." name="keyword" aria-describedby="button-addon2" value="<?= ($keyword) ? $keyword : ''; ?>">
            <div class="input-group-append">
                <button class="btn btn-outline-dark" type="submit" name="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="table-responsive pr-4 pl-4 pt-2">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Asset ID</th>
                    <th scope="col">Asset Name</th>
                    <th scope="col">Asset Type</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">User</th>
                    <th scope="col">Tanggal Masuk</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <?php if ($null === true) : ?>
                <td colspan="9" class="text-center pt-4">Data Not Found!</td>
            <?php endif; ?>
            <tbody>
                <?php $angka = 1 + (20 * ($current_page - 1)); ?>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $angka++; ?></td>
                        <td><?= $item["asset_id"]; ?></td>
                        <td><?= $item["asset_name"]; ?></td>
                        <td><?= $item["asset_type"]; ?></td>
                        <td><?= $item["jumlah"]; ?></td>
                        <td><?= $item["user"]; ?></td>
                        <td><?= $item["tgl_masuk"]; ?></td>
                        <td><?= $item["status"]; ?></td>
                        <td class="text-center">
                            <a href="/assetfunction/edit/<?= $item["id"]; ?>" class="fa fa-pencil fa-2x pr-3 text-dark editbtn"></a>
                            <a href="/assetfunction/delete/<?= $item["id"]; ?>" class="fa fa-trash fa-2x text-dark" data-toggle="modal" data-target="#modaldelete"></a>
                        </td>

                        <!-- MODALS DELETE -->
                        <div class="modal" id="modaldelete" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirmation !</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to delete this item ? <?= $item["asset_name"]; ?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" name="confirm" onclick="document.location.href = '/assetfunction/delete/<?= $item['id']; ?>'">Confirm</button>
                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODALS DELETE -->
                        <!-- MODALS INSERT TEMPLATE -->
                        <div class="modal fade" id="modalinsert" tabindex="-1" aria-labelledby="modalinsert" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= $title_insert; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/assetfunction/save/<?= $item["id"]; ?>" method="post">
                                            <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                            <input type="hidden" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here..." value="<?= old("asset_type"); ?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="asset_type">Asset Type</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here..." value="<?= old("asset_type"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("asset_type"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="user">User</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="user" id="user" placeholder="Input User Here..." value="<?= old("user"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("user"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="asset_name">Asset Name</label>
                                                <input type="text" class="form-control <?= ($validation->hasError("asset_name")) ? 'is-invalid' : ''; ?>" name="asset_name" id="asset_name" placeholder="Input Asset Name Here..." value="<?= old("asset_name"); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError("asset_name"); ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="jumlah">Jumlah</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="jumlah" id="jumlah" placeholder="Input Jumlah Here..." value="<?= old("jumlah"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("jumlah"); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="tgl_masuk">Tanggal Masuk</label>
                                                    <input type="date" class="form-control <?= ($validation->hasError("tgl_masuk")) ? 'is-invalid' : ''; ?>" name="tgl_masuk" id="tgl_masuk" placeholder="Tanggal Masuk..." value="<?= old("tgl_masuk"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("tgl_masuk"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="status">Status</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("status")) ? 'is-invalid' : ''; ?>" name="status" id="status" placeholder="Input Status Here..." value="<?= old("status"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("status"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="text-right justify-content-right">
                                                <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                                                <button type="submit" name="cancel" class="btn btn-danger"><a href="/asset/" class="text-white text-decoration-none">Cancel</a></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals Insert Templates -->
                        <!-- MODALS EDIT TEMPLATE -->
                        <div class="modal fade" id="modaledit" tabindex="-1" aria-labelledby="modaledit" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= $title_edit; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/assetfunction/update/<?= $item["id"]; ?>" method="post">
                                            <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                            <input type="hidden" class="form-control <?= ($validation->hasError("asset_name")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here...">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="asset_type">Asset Type</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here...">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("asset_type"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="user">User</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="user" id="user" placeholder="Input User Here...">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("user"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="asset_name">Asset Name</label>
                                                <input type="text" class="form-control <?= ($validation->hasError("asset_name")) ? 'is-invalid' : ''; ?>" name="asset_name" id="asset_name" placeholder="Input Asset Name Here...">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError("asset_name"); ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="jumlah">Jumlah</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="jumlah" id="jumlah" placeholder="Input Jumlah Here...">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("jumlah"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="tgl_masuk">Tanggal Masuk</label>
                                                    <input type="date" class="form-control <?= ($validation->hasError("tgl_masuk")) ? 'is-invalid' : ''; ?>" name="tgl_masuk" id="tgl_masuk" placeholder="Tanggal Masuk...">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("tgl_masuk"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="status">Status</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("status")) ? 'is-invalid' : ''; ?>" name="status" id="status" placeholder="Input Status Here...">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("status"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" name="update" class="btn btn-primary">Update Data</button>
                                            <button type="submit" name="cancel" class="btn btn-danger"><a href="/asset/" class="text-white text-decoration-none">Cancel</a></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals Edit Templates -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="pagination justify-content-left pb-2">
            <?= $pager->links("asset", "item_pagination"); ?>
        </div>
    </div>

</div>

<!-- <script>
    $(document).ready(function() {
        $('.editbtn').on('click', function() {
            $('#modaledit').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children('td').map(function() {
                return $(this).text();
            }).get();

            console.log(data);

            $('#asset_type').val(data[0]);
            $('#user').val(data[1]);
            $('#asset_name').val(data[2]);
            $('#jumlah').val(data[3]);
            $('#tgl_masuk').val(data[4]);
            $('#status').val(data[5]);
        });
    });
</script> -->

<?php $this->endSection(); ?>