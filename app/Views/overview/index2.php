<?php $this->extend("layout/template"); ?>
<?php $this->section("content") ?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3 bg-success m-2 overview">
            <div class="card-body">
                <h2 class="card-text text-center"><?= $total_asset; ?></h2>
                <h3 class="card-title text-center pb-3">Total Asset</h3>
                <p><?= $computer; ?> Computer</p>
                <p><?= $laptop; ?> Laptop</p>
                <p><?= $mobile_device; ?> Smartphone</p>
                <p><?= $monitor; ?> Monitor</p>
                <p><?= $ups; ?> Ups</p>
                <p><?= $router; ?> Router</p>
                <p><?= $server; ?> Server</p>
                <p><?= $access_point; ?> Access Point</p>
                <p><?= $switch; ?> Switch/Hub</p>
                <p><?= $keyboard; ?> Keyboard & Mouse</p>
                <p><?= $modem; ?> Modem</p>
                <p><?= $cctv; ?> Cctv</p>
                <p><?= $camera; ?> Camera</p>
                <p><?= $printer; ?> Printer</p>
                <p><?= $projector; ?> Projector</p>
            </div>
        </div>
        <div class="col-md-3 bg-warning m-2 overview">
            <div class="card-body text-center">
                <h2 class="card-text text-center"><?= $total_employee; ?></h2>
                <h3 class="card-title pb-3">Active Staff</h3>
                <p><?= $female; ?> Female</p>
                <p><?= $male; ?> Male</p>
                <p><?= $finance; ?> Finance Dept</p>
                <p><?= $it; ?> IT Dept</p>
                <p><?= $purchasing; ?> Purchasing Dept</p>
                <p><?= $pde; ?> Pde Dept</p>
                <p><?= $ppic; ?> Ppic Dept</p>
                <p><?= $hrd; ?> Hr Dept</p>
                <p><?= $security; ?> Security Dept</p>
                <p><?= $maintanence; ?> Maintanence Dept</p>
                <p><?= $warehouse; ?> Warehouse Dept</p>
                <p><?= $finishing; ?> Finishing Dept</p>
                <p><?= $quality_control; ?> Quality Control Dept</p>
            </div>
        </div>
        <div class="col-md-3 bg-danger m-2 overview">
            <div class="card-body text-center">
                <h2 class="card-text text-center">60</h2>
                <h3 class="card-title pb-3">Active Email</h3>
                <p>68 Total Email</p>
                <p>8 Deactive Email</p>
            </div>
        </div>
    </div>
</div>

<?php $this->endSection() ?>