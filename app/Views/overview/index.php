

<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<div class="container-fluid" style="background-color: #edf1f5">
    <div class="title pt-4 p-3">
        <div class="body" style="position: absolute;">
            <h5 class="card-title font-weight-bold">Overview Dashboard Data</h5>
            <h6 class="card-subtitle">Status Data PT Pacific Furniture </h6>
        </div>
        <div class="signout text-right pr-4">
            <a class="btn btn-danger fa fa-sign-out text-right" style="line-height: 25px;" href="/signin/logout"> Sign Out</a>
        </div>
    </div>
    <section class="top">
        <div class="row pl-3 pr-3 pt-3">
            <div class="col-sm">
                <div class="card text-white bg-white mb-3">
                    <div class="card-body">
                        <h5 class="card-text text-dark" style="font-size: 16px">Total Asset</h5>
                        <p class="card-text text-dark font-weight-bold" style="font-size: 30px"><?= $total_asset; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card text-white bg-white mb-3">
                    <div class="card-body">
                        <h5 class="card-text text-dark" style="font-size: 16px">Total Employee</h5>
                        <p class="card-text text-dark font-weight-bold" style="font-size: 30px"><?= $total_employee; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card text-white bg-white mb-3">
                    <div class="card-body">
                        <h5 class="card-text text-dark" style="font-size: 16px">Total Email</h5>
                        <p class="card-text text-dark font-weight-bold" style="font-size: 30px"><?= $total_email; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mid">
        <div class="row pl-3 pr-3">
            <div class="col-lg-12">
            <div class="table-responsive mt-3 bg-white pl-3">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID</th>
                            <th scope="col">Full Name</th>
                            <th scope="col">Department</th>
                            <th scope="col">Postion</th>
                            <th scope="col">Birth Date</th>
                            <th scope="col">Nomer HP</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Status</th>
                            <th scope="col">Address</th>
                            <th scope="col">Using</th>
                            <th scope="col">Sharing</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $angka = 1; ?>
                    <?php foreach ($items as $item) : ?>
                        <tr>
                            <td><?= $angka++; ?></td>
                            <td><?= $item["employee_id"]; ?></td>
                            <td><?= $item["fullname"]; ?></td>
                            <td><?= $item["department"]; ?></td>
                            <td><?= $item["position"]; ?></td>
                            <td><?= $item["birth_date"]; ?></td>
                            <td><?= $item["no_hp"]; ?></td>
                            <td><?= $item["gender"]; ?></td>
                            <td><?= $item["status"]; ?></td>
                            <td><?= $item["address"]; ?></td>
                            <td><?= $item["pake"]; ?></td>
                            <td><?= $item["file_sharing"]; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="bot">
        <div class="row pl-3 pr-3 pt-4 pb-5">
            <div class="col-lg-3 pt-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h5>Asset</h5>
                    </div>
                    <table class="table table-hover text-center">
                        <thead class="">
                            <tr>
                            <th scope="col">Asset</th>
                            <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>Computer</td><td><?= $computer; ?></td></tr>
                            <tr><td>Laptop</td><td><?= $laptop; ?></td></tr>
                            <tr><td>Smartphone</td><td><?= $mobile_device; ?></td></tr>
                            <tr><td>Router</td><td><?= $router; ?></td></tr>
                            <tr><td>Server</td><td><?= $server; ?></td></tr>
                            <tr><td>Access Point</td><td><?= $access_point; ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-3 pt-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h5>Asset</h5>
                    </div>
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                            <th scope="col">Asset</th>
                            <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>Switch</td><td><?= $switch; ?></td></tr>
                            <tr><td>CCTV</td><td><?= $cctv; ?></td></tr>
                            <tr><td>Speaker</td><td>Otto</td></tr>
                            <tr><td>Camera</td><td><?= $camera; ?></td></tr>
                            <tr><td>Printer</td><td><?= $printer; ?></td></tr>
                            <tr><td>Projector</td><td><?= $projector; ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-3 pt-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h5>Employee</h5>
                    </div>
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                            <th scope="col">Employee</th>
                            <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>Female</td><td><?= $female; ?></td></tr>
                            <tr><td>Male</td><td><?= $male; ?></td></tr>
                            <tr><td>Finance</td><td><?= $finance; ?></td></tr>
                            <tr><td>IT</td><td><?= $it; ?></td></tr>
                            <tr><td>Purchasing</td><td><?= $purchasing; ?></td></tr>
                            <tr><td>PDE</td><td><?= $pde; ?></td></tr>
                            <tr><td>PPIC</td><td><?= $ppic; ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-3 pt-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h5>Employee</h5>
                    </div>
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                            <th scope="col">Employee</th>
                            <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>HRD</td><td><?= $hrd; ?></td></tr>
                            <tr><td>Security</td><td><?= $security; ?></td></tr>
                            <tr><td>Maintanence</td><td><?= $maintanence; ?></td></tr>
                            <tr><td>Warehouse</td><td><?= $warehouse; ?></td></tr>
                            <tr><td>Finishing</td><td><?= $finishing; ?></td></tr>
                            <tr><td>Quality Control</td><td><?= $quality_control; ?>    </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->endSection(); ?>