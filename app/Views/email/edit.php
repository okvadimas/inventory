<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/emailfunction/update/<?= $items["id"]; ?>" method="post">
                        <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" class="form-control <?= ($validation->hasError("email_id")) ? 'is-invalid' : ''; ?>" name="email_id" id="email_id" placeholder="Input Asset Type Here..." value="<?= (old("email_id")) ? old("email_id") : $items["email_id"]; ?>">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email_id">Email ID</label>
                            <input type="text" class="form-control <?= ($validation->hasError("email_id")) ? 'is-invalid' : ''; ?>" name="email_id" id="email_id" placeholder="Input Email ID Here..." value="<?= (old("email_id")) ? old("email_id") : $items["email_id"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_id"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email_address">Email Address</label>
                            <input type="text" class="form-control <?= ($validation->hasError("email_address")) ? 'is-invalid' : ''; ?>" name="email_address" id="email_address" placeholder="Input Email Address Here..." value="<?= (old("email_address")) ? old("email_address") : $items["email_address"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_address"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="email_user">Email User</label>
                            <input type="text" class="form-control <?= ($validation->hasError("email_user")) ? 'is-invalid' : ''; ?>" name="email_user" id="email_user" placeholder="Input Email User Here..." value="<?= (old("email_user")) ? old("email_user") : $items["email_user"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_user"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email_pass">Email Password</label>
                            <input type="text" class="form-control <?= ($validation->hasError("email_pass")) ? 'is-invalid' : ''; ?>" name="email_pass" id="email_pass" placeholder="Input Password Here..." value="<?= (old("email_pass")) ? old("email_pass") : $items["email_pass"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_pass"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="employee_name">Employee Name</label>
                            <input type="text" class="form-control <?= ($validation->hasError("employee_name")) ? 'is-invalid' : ''; ?>" name="employee_name" id="employee_name" placeholder="Employee Name..." value="<?= (old("employee_name")) ? old("employee_name") : $items["employee_name"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("employee_name"); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="update" class="btn btn-primary">Update Data</button>
                    <button type="submit" name="cancel" class="btn btn-danger"><a href="/email/" class="text-white text-decoration-none">Cancel</a></button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>