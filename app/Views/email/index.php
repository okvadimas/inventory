<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<div class="container-fluid pt-4" style="background-color: white;">
    <div class=" title pl-4 pt-2 pb-2">
        <div class="body" style="position: absolute;">
            <h5 class="card-title font-weight-bold">Email Data Overview</h5>
            <h6 class="card-subtitle">Status inventory email </h6>
        </div>
        <div class="signout text-right pr-4">
            <a class="btn btn-danger fa fa-sign-out text-right" style="line-height: 25px;" href="/signin/logout"> Sign Out</a>
        </div>
    </div>
    <?php if (session()->getFlashData("pesan")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashData("pesan_gagal")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan_gagal") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="/email/" method="post">
        <div class="input-group mt-3 pl-4 pr-4">
            <input type="text" class="form-control btn-outline-white" placeholder="Searching Item Here..." name="keyword" aria-describedby="button-addon2" value="<?= ($keyword) ? $keyword : ''; ?>">
            <div class="input-group-append">
                <button class="btn btn-outline-dark" type="submit" name="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="table-responsive mt-3 pr-4 pl-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Email ID</th>
                    <th scope="col">Email Address</th>
                    <th scope="col">Email User</th>
                    <th scope="col">Email Password</th>
                    <th scope="col">Employee Name</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <?php if ($null === true) : ?>
                <td colspan="7" class="text-center">Data Not Found!</td>
            <?php endif; ?>
            <tbody>
                <?php $angka = 1 + (20 * ($current_page - 1)); ?>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $angka++; ?></td>
                        <td><?= $item["email_id"]; ?></td>
                        <td><?= $item["email_address"]; ?></td>
                        <td><?= $item["email_user"]; ?></td>
                        <td><?= $item["email_pass"]; ?></td>
                        <td><?= $item["employee_name"]; ?></td>
                        <td class="text-center">
                            <a href="/emailfunction/edit/<?= $item["id"]; ?>" class="fa fa-pencil fa-2x pr-3 text-dark"></a>
                            <a href="/emailfunction/delete/<?= $item["id"]; ?>" class="fa fa-trash fa-2x text-dark" data-toggle="modal" data-target="#modaldelete"></a>
                        </td>
                        <!-- MODALS DELETE -->
                        <div class="modal" id="modaldelete" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirmation !</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to delete this item ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-primary" onclick="document.location.href = '/emailfunction/delete/<?= $item['id']; ?>'">Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODALS DELETE -->
                        <!-- MODALS INSERT TEMPLATE -->
                        <div class="modal fade" id="modalinsert" tabindex="-1" aria-labelledby="modalinsert" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= $title_insert; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/emailfunction/save/<?= $item["id"]; ?>" method="post">
                                            <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                            <input type="hidden" class="form-control <?= ($validation->hasError("email_id")) ? 'is-invalid' : ''; ?>" name="email_id" id="email_id" placeholder="Input Asset Type Here..." value="<?= old("email_id"); ?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="email_id">Email _ID</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("email_id")) ? 'is-invalid' : ''; ?>" name="email_id" id="email_id" placeholder="Input Email ID Here..." value="<?= old("email_id"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("email_id"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="email_address">Email Address</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("email_address")) ? 'is-invalid' : ''; ?>" name="email_address" id="email_address" placeholder="Input Email Address Here..." value="<?= old("email_address"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("email_address"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="email_user">Email User</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("email_user")) ? 'is-invalid' : ''; ?>" name="email_user" id="email_user" placeholder="Input Email User Here..." value="<?= old("email_user"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("email_user"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="email_pass">Email Password</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("email_pass")) ? 'is-invalid' : ''; ?>" name="email_pass" id="email_pass" placeholder="Input Password Here..." value="<?= old("email_pass"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("email_pass"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="employee_name">Employee Name</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("employee_name")) ? 'is-invalid' : ''; ?>" name="employee_name" id="employee_name" placeholder="Input Employee Name Here..." value="<?= old("employee_name"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("employee_name"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right justify-content-right mt-4">
                                                <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                                                <button type="submit" name="cancel" class="btn btn-danger"><a href="/email/" class="text-white text-decoration-none">Cancel</a></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals Insert Templates -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="pagination justify-content-left pb-2">
            <?= $pager->links("email", "item_pagination"); ?>
        </div>
    </div>

</div>

<!-- MODALS TEMPLATE -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="inputAddress">Asset Code</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2">Asset Name</label>
                        <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Asset Type</label>
                            <input type="email" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">User</label>
                            <input type="text" class="form-control" id="inputPassword4">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Divisi</label>
                            <input type="email" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location</label>
                            <input type="text" class="form-control" id="inputPassword4">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>

<?php $this->endSection(); ?>