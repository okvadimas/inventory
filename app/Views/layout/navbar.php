<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pr-3">
                    <a class="nav-link text-white" href="/">Asset</a>
                </li>
                <li class="nav-item pr-3">
                    <a class="nav-link text-white" href="/employee/">Employee</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="/access/">Access</a>
                </li>
            </ul>
        </div>
        <a class="navbar-brand mx-auto font-weight-bold" href="#">Pacific Furniture</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item pr-3">
                    <a class="nav-link text-white" href="/email">Email</a>
                </li>
                <li class="nav-item pr-3">
                    <a class="nav-link text-white" data-toggle="modal" data-target="#modalinsert">Insert</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="/overview/">Overview</a>
                </li>
            </ul>
        </div>
    </div>

</nav>