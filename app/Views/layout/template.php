<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://use.fontawesome.com/f6584c4c37.js"></script>

    <title>Inventory</title>
</head>

<body>

    <?= $this->include("/layout/navbar"); ?>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <!-- <script>
        $(document).ready(function() {
            $('.editbtn').on('click', function() {
                $('#modaledit').modal('show');
                $tr = $(this).closest('tr');

                var data = $tr.children('td').map(function() {
                    return $(this).text();
                }).get();

                console.log(data);

                $('asset_type').val(data[0]);
                $('user').val(data[0]);
                $('asset_name').val(data[0]);
                $('jumlah').val(data[0]);
                $('tgl_masuk').val(data[0]);
                $('status').val(data[0]);
            });
        });
    </script> -->

</body>

<?php $this->renderSection("content"); ?>

<footer class="bg-dark">
    <div class="logofooter">
        <ul class="term justify-content-center text-white">
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " class="mr-2">Dimas</a></li>
            <li>
                <p>|</p>
            </li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " class="ml-2 mr-2">Anam</a></li>
            <li>
                <p>|</p>
            </li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " class="mr-2 ml-2">Arya</a></li>
            <li>
                <p>|</p>
            </li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " class="ml-2 mr-2">Tope</a></li>
        </ul>
        <ul class="logo justify-content-center">
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " target="_blank"><i class="fa fa-whatsapp"></i></a></li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " target="_blank"><i class="fa fa-whatsapp"></i></a></li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " target="_blank"><i class="fa fa-whatsapp"></i></a></li>
            <li><a href="https://api.whatsapp.com/send?phone=6288211234125&text= " target="_blank"><i class="fa fa-whatsapp"></i></a></li>
        </ul>
        <ul class="copy justify-content-center">
            <li>
                <p>Copyright © 2020 PT Pacific Furniture All rights reserved.</p>
            </li>
        </ul>
    </div>
</footer>

</html>