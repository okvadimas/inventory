<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<div class="container-fluid pt-4" style="background-color: white;">
    <div class=" title pl-4 pt-2 pb-2">
        <div class="body" style="position: absolute;">
            <h5 class="card-title font-weight-bold">Employee Data Overview</h5>
            <h6 class="card-subtitle">Status data employee </h6>
        </div>
        <div class="signout text-right pr-4">
            <a class="btn btn-danger fa fa-sign-out text-right" style="line-height: 25px;" href="/signin/logout"> Sign Out</a>
        </div>
    </div>
    <?php if (session()->getFlashData("pesan")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashData("pesan_gagal")) : ?>
        <div class="alert alert-success alert-dismissible fade show ml-4 mr-4 text-center" role="alert">
            <?= session()->getFlashData("pesan_gagal") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="/employee/" method="post">
        <div class="input-group mt-3 pl-4 pr-4">
            <input type="text" class="form-control btn-outline-white" placeholder="Searching Item Here..." name="keyword" aria-describedby="button-addon2" value="<?= ($keyword) ? $keyword : ''; ?>">
            <div class="input-group-append">
                <button class="btn btn-outline-dark" type="submit" name="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="table-responsive mt-3 pr-4 pl-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID</th>
                    <th scope="col" class="w-500">Full Name</th>
                    <th scope="col">Department</th>
                    <th scope="col">Postion</th>
                    <th scope="col">Birth Date</th>
                    <th scope="col">Nomer HP</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Status</th>
                    <th scope="col">Address</th>
                    <th scope="col">Using</th>
                    <th scope="col">File Sharing</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <?php if ($null === true) : ?>
                <td colspan="13" class="text-center">Data Not Found!</td>
            <?php endif; ?>
            <tbody>
                <?php $angka = 1 + (20 * ($current_page - 1)); ?>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $angka++; ?></td>
                        <td><?= $item["employee_id"]; ?></td>
                        <td><?= $item["fullname"]; ?></td>
                        <td><?= $item["department"]; ?></td>
                        <td><?= $item["position"]; ?></td>
                        <td><?= $item["birth_date"]; ?></td>
                        <td><?= $item["no_hp"]; ?></td>
                        <td><?= $item["gender"]; ?></td>
                        <td><?= $item["status"]; ?></td>
                        <td><?= $item["address"]; ?></td>
                        <td><?= $item["pake"]; ?></td>
                        <td><?= $item["file_sharing"]; ?></td>
                        <td class="text-center">
                            <a href="/employeefunction/edit/<?= $item["id"]; ?>" class="fa fa-pencil fa-1x pr-3 text-dark"></a>
                            <a href="/employeefunction/delete/<?= $item["id"]; ?>" class="fa fa-trash fa-1x text-dark" data-toggle="modal" data-target="#modaldelete"></a>
                        </td>
                        <!-- MODALS DELETE -->
                        <div class="modal" id="modaldelete" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirmation !</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to delete this item ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-primary" onclick="document.location.href = '/employeefunction/delete/<?= $item['id']; ?>'">Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODALS DELETE -->
                        <!-- MODALS INSERT TEMPLATE -->
                        <div class="modal fade" id="modalinsert" tabindex="-1" aria-labelledby="modalinsert" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= $title_insert; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/employeefunction/save" method="post">
                                            <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                            <input type="hidden" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="fullname" id="fullname" placeholder="Input Fullname Here..." value="<?= old("asset_type"); ?>">

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="asset_type">Employee ID</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("employee_id")) ? 'is-invalid' : ''; ?>" name="employee_id" id="employee_id" placeholder="Input Employee ID Here..." value="<?= old("asset_type"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("employee_id"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="asset_type">Full Name</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("fullname")) ? 'is-invalid' : ''; ?>" name="fullname" id="fullname" placeholder="Input Fullname Here..." value="<?= old("asset_type"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("fullname"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="jumlah">Department</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="department" id="department" placeholder="Input Department Here..." value="<?= old("jumlah"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("department"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="user">Position</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="position" id="position" placeholder="Input Position Here..." value="<?= old("user"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("position"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="tgl_masuk">No Handphone</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("tgl_masuk")) ? 'is-invalid' : ''; ?>" name="no_hp" id="no_hp" placeholder="Input No Handphone Here..." value="<?= old("tgl_masuk"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("no_hp"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="jumlah">Birth Date</label>
                                                    <input type="date" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="birth_date" id="birth_date" placeholder="Input Birth Date Here..." value="<?= old("jumlah"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("birth_date"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="user">File Sharing</label>
                                                    <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="file_sharing" id="file_sharing" placeholder="Input File Sharing Here..." value="<?= old("user"); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("file_sharing"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="asset_type">Address</label>
                                                <input type="text" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="address" id="address" placeholder="Input Address Here..." value="<?= old("asset_type"); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError("address"); ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="jumlah">Use</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="pake" id="pake1" value="Mobile Device">
                                                        <label class="form-check-label" for="pake1">Mobile Device</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="pake" id="pake2" value="Laptop">
                                                        <label class="form-check-label" for="pake2">Laptop</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="pake" id="pake3" value="Computer">
                                                        <label class="form-check-label" for="pake3">Computer</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("pake"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for=" user">Status</label><br>
                                                    <div class="form-check form-check-inline hide-radio">
                                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Active">
                                                        <label class="form-check-label" for="inlineRadio1">Active</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Non-Active">
                                                        <label class="form-check-label" for="inlineRadio2">Deactive</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("status"); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="user">Gender</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="Female">
                                                        <label class="form-check-label" for="inlineRadio3">Female</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio4" value="Male">
                                                        <label class="form-check-label" for="inlineRadio4">Male</label>
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError("gender"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right justify-content-right mt-4">
                                                <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                                                <button type="submit" name="cancel" class="btn btn-danger"><a href="/employee/" class="text-white text-decoration-none">Cancel</a></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals Insert Templates -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="pagination justify-content-left pb-2">
            <?= $pager->links("employee", "item_pagination"); ?>
        </div>
    </div>

</div>

<?php $this->endSection(); ?>