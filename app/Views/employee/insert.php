<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/employeefunction/save" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="fullname" id="fullname" placeholder="Input Fullname Here..." value="<?= old("asset_type"); ?>">                 

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="asset_type">Employee ID</label>
                            <input type="text" class="form-control <?= ($validation->hasError("employee_id")) ? 'is-invalid' : ''; ?>" name="employee_id" id="employee_id" placeholder="Input Employee ID Here..." value="<?= old("asset_type"); ?>">
                            <div class="invalid-feedback"> 
                                <?= $validation->getError("employee_id"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="asset_type">Full Name</label>
                            <input type="text" class="form-control <?= ($validation->hasError("fullname")) ? 'is-invalid' : ''; ?>" name="fullname" id="fullname" placeholder="Input Fullname Here..." value="<?= old("asset_type"); ?>">
                            <div class="invalid-feedback"> 
                                <?= $validation->getError("fullname"); ?>
                            </div>
                    </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="jumlah">Department</label>
                            <input type="text" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="department" id="department" placeholder="Input Department Here..." value="<?= old("jumlah"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("department"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="user">Position</label>
                            <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="position" id="position" placeholder="Input Position Here..." value="<?= old("user"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("position"); ?> 
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tgl_masuk">No Handphone</label>
                            <input type="text" class="form-control <?= ($validation->hasError("tgl_masuk")) ? 'is-invalid' : ''; ?>" name="no_hp" id="no_hp" placeholder="Input No Handphone Here..." value="<?= old("tgl_masuk"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("no_hp"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="jumlah">Birth Date</label>
                            <input type="date" class="form-control <?= ($validation->hasError("jumlah")) ? 'is-invalid' : ''; ?>" name="birth_date" id="birth_date" placeholder="Input Birth Date Here..." value="<?= old("jumlah"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("birth_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user">File Sharing</label>
                            <input type="text" class="form-control <?= ($validation->hasError("user")) ? 'is-invalid' : ''; ?>" name="file_sharing" id="file_sharing" placeholder="Input File Sharing Here..." value="<?= old("user"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("file_sharing"); ?> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="asset_type">Address</label>
                        <input type="text" class="form-control <?= ($validation->hasError("asset_type")) ? 'is-invalid' : ''; ?>" name="address" id="address" placeholder="Input Address Here..." value="<?= old("asset_type"); ?>">
                        <div class="invalid-feedback"> 
                            <?= $validation->getError("address"); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="jumlah">Use</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="pake" id="pake1" value="Mobile Device">
                                <label class="form-check-label" for="pake1">Mobile Device</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="pake" id="pake2" value="Laptop">
                                <label class="form-check-label" for="pake2">Laptop</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="pake" id="pake3" value="Computer">
                                <label class="form-check-label" for="pake3">Computer</label>
                            </div>
                            <div class="invalid-feedback">
                                <?= $validation->getError("pake"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="user">Status</label><br>
                            <div class="form-check form-check-inline hide-radio">
                                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Single">
                                <label class="form-check-label" for="inlineRadio1">Single</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Married">
                                <label class="form-check-label" for="inlineRadio2">Married</label>
                            </div>                         
                            <div class="invalid-feedback">
                                <?= $validation->getError("status"); ?> 
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="user">Gender</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="Female">
                                <label class="form-check-label" for="inlineRadio3">Female</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio4" value="Male">
                                <label class="form-check-label" for="inlineRadio4">Male</label>
                            </div>                          
                            <div class="invalid-feedback">
                                <?= $validation->getError("gender"); ?> 
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="tambah" class="btn btn-primary">Insert Data</button>
                    <button type="submit" name="cancel" class="btn btn-danger"><a href="/employee/" class="text-white text-decoration-none">Cancel</a></button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>