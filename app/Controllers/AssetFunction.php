<?php

namespace App\Controllers;

use \App\Models\Models;

class AssetFunction extends BaseController
{
	protected $itemModel;

	public function __construct()
	{
		$this->itemModel = new Models();
	}

	public function tambah()
	{
		$item = $this->itemModel->orderBy("id", "DESC")->first();
		// dd($item);
		$data = [
			"title" => "Insert Page",
			"validation" => \Config\Services::validation(),
			"items" => $item,
		];
		return view("/asset/insert", $data);
	}

	public function save($id)
	{
		if (!$this->validate([
			"asset_name" => "required",
			"asset_id" => "is_unique[asset.asset_id]",
			"asset_type" => "required",
			"jumlah" => "required",
			"user" => "required",
			"status" => "required",
		])) {
			$validation = \Config\Services::validation();
			session()->setFlashdata("pesan_gagal", "Failed Insert New Data !");
			// dd($validation);
			return redirect()->to("/assetfunction/tambah")->withInput()->with("validation", $validation);
		};

		// dd($this->request->getVar());
		// var_dump($this->request->getVar());

		if (strlen($id) === 1) {
			$id_convert = "A0000" . $id;
		} elseif (strlen($id) === 2) {
			$id_convert = "A000" . $id;
		} elseif (strlen($id) === 3) {
			$id_convert = "A00" . $id;
		} elseif (strlen($id) === 4) {
			$id_convert = "A0" . $id;
		} elseif (strlen($id) === 5) {
			$id_convert = "A" . $id;
		};

		// dd($id_convert);

		$this->itemModel->save([
			"asset_id" => $id_convert,
			"asset_type" => $this->request->getVar("asset_type"),
			"asset_name" => $this->request->getVar("asset_name"),
			"jumlah" => $this->request->getVar("jumlah"),
			"tgl_masuk" => $this->request->getVar("tgl_masuk"),
			"user" => $this->request->getVar("user"),
			"status" => $this->request->getVar("status"),
		]);
		// dd($this->request->getVar());


		session()->setFlashdata("pesan", "Success Insert New Data !");

		return redirect()->to("/asset");
	}

	public function edit($id)
	{
		// dd($this->request->get_var("id"));
		$item = $this->itemModel->where(["id" => $id])->first();
		$data = [
			"items" => $item,
			"title" => "Edit Page",
			"validation" => \Config\Services::validation(),
		];
		return view('/asset/edit', $data);
	}

	public function update($id)
	{
		// dd($this->request->getVar());
		$itemLama = $this->itemModel->where(["id" => $id])->first();
		// if ($itemLama["asset_id"] == $this->request->getVar("asset_id")) 
		// {
		// 	$rule = "required";
		// } else {
		// 	$rule ="required|is_unique[asset.asset_id]";
		// }

		if (!$this->validate([
			"asset_name" => "required",
			"asset_type" => "required",
			"jumlah" => "required",
			"user" => "required",
			"status" => "required",
		])) {
			$validation = \Config\Services::validation();
			session()->setFlashdata("pesan_gagal", "Failed Insert New Data");
			return redirect()->to("/assetfunction/edit/" . $id)->withInput()->with("validation", $validation);
		};

		$this->itemModel->save([
			"id" => $id,
			"asset_id" => $itemLama["asset_id"],
			"asset_type" => $this->request->getVar("asset_type"),
			"asset_name" => $this->request->getVar("asset_name"),
			"jumlah" => $this->request->getVar("jumlah"),
			"tgl_masuk" => $this->request->getVar("tgl_masuk"),
			"user" => $this->request->getVar("user"),
			"status" => $this->request->getVar("status"),
		]);

		session()->setFlashdata("pesan", "Success Edit Existing Data");

		return redirect()->to("/asset");
	}

	public function delete($id)
	{
		dd($id);

		$this->itemModel->delete($id);
		return redirect()->to("/asset");
	}
}
