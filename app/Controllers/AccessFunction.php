<?php namespace App\Controllers;

use \App\Models\ModelsAccess;

class AccessFunction extends BaseController
{
	protected $itemModel;

	public function __construct() 
	{
		$this->itemModel = new ModelsAccess();
    }
    
    public function tambah()
    {
		$item = $this->itemModel->orderBy("id", "DESC")->first();
		// dd($item);
		$data = [
			"title" => "Insert Page",
			"validation" => \Config\Services::validation(),
			"items" => $item,
		];
        return view("/access/insert", $data);
	}
	
	public function save($id)
    {
		// dd($this->request->getVar());
        if (!$this->validate([
			"use_access" => "required",
			"email_access" => "is_unique[access.id]",
			"inet_access" => "required",
			"printer_access" => "required",
			"usb_access" => "required",
            "file_sharing_access" => "required",
			"domain_access" => "required",
			"domain_pass" => "required",
            "anydesk" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data !");
            // dd($validation);
            return redirect()->to("/accessfunction/tambah")->withInput()->with("validation", $validation);
        };

		// dd($this->request->getVar());
		// var_dump($this->request->getVar());

		// if(strlen($id) === 1) {
		// 	$id_convert = "A0000" . $id;
		// } elseif(strlen($id) === 2) {
		// 	$id_convert = "A000" . $id;
		// } elseif(strlen($id) === 3) {
		// 	$id_convert = "A00" . $id;
		// } elseif(strlen($id) === 4) {
		// 	$id_convert = "A0" . $id;
		// } elseif(strlen($id) === 5) {
		// 	$id_convert = "A" . $id;
		// };

		// dd($id_convert);

		$this->itemModel->save([
			"use_access" => $this->request->getVar("use_access"),
			"email_access" => $this->request->getVar("email_access"),
            "inet_access" => $this->request->getVar("inet_access"),
            "printer_access" => $this->request->getVar("printer_access"),
            "usb_access" => $this->request->getVar("usb_access"),
            "file_sharing_access" => $this->request->getVar("file_sharing_access"),
			"domain_access" => $this->request->getVar("domain_access"),
			"domain_pass" => $this->request->getVar("domain_pass"),
            "anydesk" => $this->request->getVar("anydesk"),
		]);

        session()->setFlashdata("pesan", "Success Insert New Data !");

        return redirect()->to("/access");
    }

	public function edit($id)
	{
        // dd($this->request->getVar());
		$item = $this->itemModel->where(["id" => $id])->first();
		$data = [
			"items" => $item,
			"title" => "Edit Page",
			"validation" => \Config\Services::validation(),
		];
		return view('/access/edit', $data);
	}

	public function update($id)
	{
		// dd($this->request->getVar());
		$itemLama = $this->itemModel->where(["id" => $id])->first();
		if ($itemLama["email_access"] == $this->request->getVar("email_access")) 
		{
			$rule = "required";
		} else {
			$rule ="required|is_unique[asset.email_access]";
		}

		if (!$this->validate([
			"use_access" => "required",
			"email_access" => $rule,
			"inet_access" => "required",
			"printer_access" => "required",
            "usb_access" => "required",
            "file_sharing_access" => "required",
			"domain_access" => "required",
			"domain_pass" => "required",
            "anydesk" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data");
            return redirect()->to("/assetfunction/edit/" . $id)->withInput()->with("validation", $validation);
        };

        $this->itemModel->save([
			"id" => $id,
			"use_access" => $itemLama["use_access"],
			"email_access" => $this->request->getVar("email_access"),
            "inet_access" => $this->request->getVar("inet_access"),
            "printer_access" => $this->request->getVar("printer_access"),
            "usb_access" => $this->request->getVar("usb_access"),
            "file_sharing_access" => $this->request->getVar("file_sharing_access"),
			"domain_access" => $this->request->getVar("domain_access"),
			"domain_pass" => $this->request->getVar("domain_pass"),
            "anydesk" => $this->request->getVar("anydesk"),
		]);

        session()->setFlashdata("pesan", "Success Insert New Data");

        return redirect()->to("/access");
	}

	public function delete($id)
	{
		$this->itemModel->delete($id);
		return redirect()->to("/access");
	}

}
