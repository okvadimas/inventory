<?php

namespace App\Controllers;

use \App\Models\Models;
use phpDocumentor\Reflection\Types\Null_;

class Asset extends BaseController
{
	protected $itemModel;

	public function __construct()
	{
		$this->itemModel = new Models();
	}

	public function index()
	{
		if (!session()->get("username")) {
			return redirect()->to("/signin/");
		}

		$current_page = $this->request->getVar("page_asset") ? $this->request->getVar("page_asset") : 1;

		$keyword = $this->request->getVar("keyword");

		if ($keyword) {
			$item = $this->itemModel->search($keyword)->orderBy("asset_id", "DESC")->paginate(20, "asset");
		} else {
			$item = $this->itemModel->orderBy("asset_id", "desc")->paginate(20, "asset");
		};

		if (empty($item)) :
			$null = true;
		else :
			$null = false;
		endif;

		// dd($null);

		$pager = $this->itemModel->pager;

		$data = [
			"items" => $item,
			"aktif" => "/assetfunction/tambah",
			"keyword" => $this->request->getVar("keyword"),
			"pager" => $pager,
			"current_page" => $current_page,
			"validation" => \Config\Services::validation(),
			"title_insert" => "Insert View",
			"title_edit" => "Edit View",
			"null" => $null,
		];
		return view('/asset/index.php', $data);
	}
}
