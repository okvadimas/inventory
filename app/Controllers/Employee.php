<?php

namespace App\Controllers;

use \App\Models\ModelsEmployee;

class Employee extends BaseController
{
	protected $itemModel;

	public function __construct()
	{
		$this->itemModel = new ModelsEmployee();
	}

	public function index()
	{
		if (!session()->get("username")) {
			return redirect()->to("/signin/");
		}

		$current_page = $this->request->getVar("page_employee") ? $this->request->getVar("page_employee") : 1;

		$keyword = $this->request->getVar("keyword");

		if ($keyword) {
			$item = $this->itemModel->search($keyword)->orderBy("employee_id", "ASC")->paginate(20, "employee");
		} else {
			$item = $this->itemModel->orderBy("employee_id", "ASC")->paginate(20, "employee");
		};

		if (empty($item)) :
			$null = true;
		else :
			$null = false;
		endif;

		$pager = $this->itemModel->pager;

		$data = [
			"items" => $item,
			"aktif" => "/employeefunction/tambah",
			"keyword" => $this->request->getVar("keyword"),
			"current_page" => $current_page,
			"pager" => $pager,
			"validation" => \Config\Services::validation(),
			"title_insert" => "Insert View",
			"title_edit" => "Edit View",
			"null" => $null,
		];
		return view('/employee/index.php', $data);
	}
}
