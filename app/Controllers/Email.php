<?php

namespace App\Controllers;

use \App\Models\ModelsEmail;
use \App\Libraries\zend;

class Email extends BaseController
{
	protected $itemModel;

	public function __construct()
	{
		$this->itemModel = new ModelsEmail();
	}

	public function index()
	{
		$this->load->library('zend');
		// $this->zend->load('Zend/Barcode');

		if (!session()->get("username")) {
			return redirect()->to("/signin/");
		}

		$current_page = $this->request->getVar("page_email") ? $this->request->getVar("page_email") : 1;

		$keyword = $this->request->getVar("keyword");

		if ($keyword) {
			$item = $this->itemModel->search($keyword)->orderBy("email_id", "ASC")->paginate(20, "email");
		} else {
			$item = $this->itemModel->orderBy("email_id", "ASC")->paginate(20, "email");
		};

		if (empty($item)) :
			$null = true;
		else :
			$null = false;
		endif;

		$pager = $this->itemModel->pager;

		$data = [
			"items" => $item,
			"aktif" => "/emailfunction/tambah",
			"keyword" => $this->request->getVar("keyword"),
			"pager" => $pager,
			"current_page" => $current_page,
			"validation" => \Config\Services::validation(),
			"title_insert" => "Insert View",
			"title_edit" => "Edit View",
			"null" => $null,
		];
		return view('/email/index.php', $data);
	}
}
