<?php namespace App\Controllers;

use \App\Models\ModelsEmail;

class EmailFunction extends BaseController
{
	protected $itemModel;

	public function __construct() 
	{
		$this->itemModel = new ModelsEmail();
	}
    
    public function tambah()
    {
		$item = $this->itemModel->orderBy("id", "DESC")->first();
		// dd($item);
		$data = [
			"title" => "Insert Page",
			"validation" => \Config\Services::validation(),
			"items" => $item,
		];
        return view("/email/insert", $data);
	}
	
	public function save()
    {
        // dd($this->request->getVar());
        // dd($id);

        if (!$this->validate([
			"email_id" => "required|is_unique[email.email_id]",
			"email_address" => "required|is_unique[email.email_address]",
			"email_user" => "required",
			"email_pass" => "required",
			"employee_name" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data !");
            // dd($validation);
            return redirect()->to("/emailfunction/tambah")->withInput()->with("validation", $validation);
        };

		// dd($this->request->getVar());
		// var_dump($this->request->getVar());

		// if(strlen($id) === 1) {
		// 	$id_convert = "00000" . $id;
		// } elseif(strlen($id) === 2) {
		// 	$id_convert = "0000" . $id;
		// } elseif(strlen($id) === 3) {
		// 	$id_convert = "000" . $id;
		// } elseif(strlen($id) === 4) {
		// 	$id_convert = "00" . $id;
		// } elseif(strlen($id) === 5) {
		// 	$id_convert = "0" . $id;
		// };

		// dd($id_convert);

		$this->itemModel->save([
			"email_id" => $this->request->getVar("email_id"),
			"email_address" => $this->request->getVar("email_address"),
            "email_user" => $this->request->getVar("email_user"),
            "email_pass" => $this->request->getVar("email_pass"),
            "employee_name" => $this->request->getVar("employee_name"),
		]);

        session()->setFlashdata("pesan", "Success Insert New Data !");

        return redirect()->to("/email");
    }

	public function edit($id)
	{
        // dd($this->request->get_var("id"));
		$item = $this->itemModel->where(["id" => $id])->first();
		$data = [
			"items" => $item,
			"title" => "Edit Page",
			"validation" => \Config\Services::validation(),
		];
		return view('/email/edit', $data);
	}

	public function update($id)
	{
		$itemLama1 = $this->itemModel->where(["id" => $id])->first();
		if ($itemLama1["email_id"] == $this->request->getVar("email_id")) 
		{
			$rule1 = "required";
		} else {
			$rule1 ="required|is_unique[email.email_id]";
		}

		$itemLama2 = $this->itemModel->where(["id" => $id])->first();
		if ($itemLama2["email_address"] == $this->request->getVar("email_address")) 
		{
			$rule2 = "required";
		} else {
			$rule2 ="required|is_unique[email.email_address]";
		}

		if (!$this->validate([
            "email_id" => $rule1,
			"email_address" => $rule2,
			"email_user" => "required",
			"email_pass" => "required",
			"employee_name" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data");
            return redirect()->to("/emailfunction/edit/" . $id)->withInput()->with("validation", $validation);
        };

        $this->itemModel->save([
			"id" => $id,
			"email_id" => $this->request->getVar("email_id"),
			"email_address" => $this->request->getVar("email_address"),
            "email_user" => $this->request->getVar("email_user"),
            "email_pass" => $this->request->getVar("email_pass"),
            "employee_name" => $this->request->getVar("employee_name"),
		]);

        session()->setFlashdata("pesan", "Success Insert New Data");

        return redirect()->to("/email");
	}

	public function delete($id)
	{
		dd($id);
		$this->itemModel->delete($id);
		return redirect()->to("/email/");
	}

}
