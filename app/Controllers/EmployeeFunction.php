<?php namespace App\Controllers;

use \App\Models\ModelsEmployee;

class EmployeeFunction extends BaseController
{
	protected $itemModel;

	public function __construct() 
	{
		$this->itemModel = new ModelsEmployee();
	}
    
    public function tambah()
    {
		$item = $this->itemModel->orderBy("id", "DESC")->first();
		// dd($item);
		$data = [
			"title" => "Insert Page",
			"validation" => \Config\Services::validation(),
			"items" => $item,
		];
        return view("/employee/insert", $data);
	}
	
	public function save()
    {
        // dd($this->request->getVar());
        // dd($id);

        if (!$this->validate([
			"employee_id" => "required|is_unique[employee.employee_id]",
			"fullname" => "required",
			"department" => "required",
			"position" => "required",
            "gender" => "required",
			"address" => "required",
			"pake" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data !");
            // dd($validation);
            return redirect()->to("/employeefunction/")->withInput()->with("validation", $validation);
        };

		// dd($this->request->getVar());
		// var_dump($this->request->getVar());

		// if(strlen($id) === 1) {
		// 	$id_convert = "00000" . $id;
		// } elseif(strlen($id) === 2) {
		// 	$id_convert = "0000" . $id;
		// } elseif(strlen($id) === 3) {
		// 	$id_convert = "000" . $id;
		// } elseif(strlen($id) === 4) {
		// 	$id_convert = "00" . $id;
		// } elseif(strlen($id) === 5) {
		// 	$id_convert = "0" . $id;
		// };

		// dd($id_convert);

		$this->itemModel->save([
			"employee_id" => $this->request->getVar("employee_id"),
			"fullname" => $this->request->getVar("fullname"),
            "department" => $this->request->getVar("department"),
            "position" => $this->request->getVar("position"),
            "birth_date" => $this->request->getVar("birth_date"),
            "no_hp" => $this->request->getVar("no_hp"),
            "gender" => $this->request->getVar("gender"),
            "status" => $this->request->getVar("status"),
            "address" => $this->request->getVar("address"),
            "pake" => $this->request->getVar("pake"),
            "file_sharing" => $this->request->getVar("file_sharing"),
		]);
				// dd($this->request->getVar());


        session()->setFlashdata("pesan", "Success Insert New Data !");

        return redirect()->to("/employee");
    }

	public function edit($id)
	{
        // dd($this->request->get_var("id"));
		$item = $this->itemModel->where(["id" => $id])->first();
		$data = [
			"items" => $item,
			"title" => "Edit Page",
			"validation" => \Config\Services::validation(),
		];
		return view('/employee/edit', $data);
	}

	public function update($id)
	{
		$itemLama = $this->itemModel->where(["id" => $id])->first();
		if ($itemLama["employee_id"] == $this->request->getVar("employee_id")) 
		{
			$rule = "required";
		} else {
			$rule ="required|is_unique[employee.employee_id]";
		}

		if (!$this->validate([
            "employee_id" => $rule,
			"fullname" => "required",
			"department" => "required",
			"position" => "required",
            "gender" => "required",
			"address" => "required",
			"pake" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Edit Existing Data");
            return redirect()->to("/employeefunction/edit/" . $id)->withInput()->with("validation", $validation);
        };

        $this->itemModel->save([
			"id" => $id,
			"employee_id" => $this->request->getVar("employee_id"),
			"fullname" => $this->request->getVar("fullname"),
            "department" => $this->request->getVar("department"),
            "position" => $this->request->getVar("position"),
            "birth_date" => $this->request->getVar("birth_date"),
            "no_hp" => $this->request->getVar("no_hp"),
            "gender" => $this->request->getVar("gender"),
            "status" => $this->request->getVar("status"),
            "address" => $this->request->getVar("address"),
            "pake" => $this->request->getVar("pake"),
            "file_sharing" => $this->request->getVar("file_sharing"),
		]);

        session()->setFlashdata("pesan", "Success Edit Existing Data");

        return redirect()->to("/employee");
	}

	public function delete($id)
	{
		dd($id);
		$this->itemModel->delete($id);
		return redirect()->to("/employee/");
	}

}
