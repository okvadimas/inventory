<?php

namespace App\Controllers;

use \App\Models\Models;
use \App\Models\ModelsEmail;
use \App\Models\ModelsEmployee;

class Overview extends BaseController
{
    protected $itemModel_asset;
    protected $itemModel_employee;
    protected $itemModel_email;

    public function __construct()
    {
        $this->itemModel_asset = new Models();
        $this->itemModel_employee = new ModelsEmployee();
        $this->itemModel_email = new ModelsEmail();
    }

    public function index()
    {
        if (!session()->get("username")) {
            return redirect()->to("/signin/");
        }

        $total_asset = count($this->itemModel_asset->findAll());
        $computer = count($this->itemModel_asset->like("asset_type", "computer")->findAll());
        $laptop = count($this->itemModel_asset->like("asset_type", "laptop")->findAll());
        $mobile_device = count($this->itemModel_asset->like("asset_type", "mobile device")->findAll());
        $monitor = count($this->itemModel_asset->like("asset_type", "monitor")->findAll());
        $ups = count($this->itemModel_asset->like("asset_type", "ups")->findAll());
        $router = count($this->itemModel_asset->like("asset_type", "router")->findAll());
        $server = count($this->itemModel_asset->like("asset_type", "server")->findAll());
        $access_point = count($this->itemModel_asset->like("asset_type", "access point")->findAll());
        $switch = count($this->itemModel_asset->like("asset_type", "switch")->orLike('asset_type', 'hub')->findAll());
        $keyboard = count($this->itemModel_asset->like("asset_type", "keyboard")->orLike('asset_type', 'mouse')->findAll());
        $modem = count($this->itemModel_asset->like("asset_type", "modem")->findAll());
        $cctv = count($this->itemModel_asset->like("asset_type", "cctv")->findAll());
        $camera = count($this->itemModel_asset->like("asset_type", "camera")->findAll());
        $printer = count($this->itemModel_asset->like("asset_type", "printer")->findAll());
        $projector = count($this->itemModel_asset->like("asset_type", "projector")->findAll());

        $total_employee = count($this->itemModel_employee->findAll());
        $female = count($this->itemModel_employee->like("gender", "female")->findAll());
        $male = count($this->itemModel_employee->like("gender", "male")->findAll());
        $finance = count($this->itemModel_employee->like("department", "finance")->findAll());
        $it = count($this->itemModel_employee->like("department", "it")->findAll());
        $purchasing = count($this->itemModel_employee->like("department", "purchasing")->findAll());
        $pde = count($this->itemModel_employee->like("department", "pde")->findAll());
        $ppic = count($this->itemModel_employee->like("department", "ppic")->findAll());
        $hrd = count($this->itemModel_employee->like("department", "hrd")->findAll());
        $security = count($this->itemModel_employee->like("department", "security")->findAll());
        $maintanence = count($this->itemModel_employee->like("department", "maintanence")->findAll());
        $warehouse = count($this->itemModel_employee->like("department", "warehouse")->findAll());
        $finishing = count($this->itemModel_employee->like("department", "finishing")->findAll());
        $quality_control = count($this->itemModel_employee->like("department", "quality control")->findAll());

        $total_email = count($this->itemModel_email->like("email_address", ".com")->orLike("email_address", ".id")->findAll());

        $item = $this->itemModel_employee->orderBy("employee_id", "ASC")->paginate(5, "employee");

        $data = [
            "computer" => $computer,
            "total_asset" => $total_asset,
            "laptop" => $laptop,
            "mobile_device" => $mobile_device,
            "monitor" => $monitor,
            "ups" => $ups,
            "router" => $router,
            "server" => $server,
            "access_point" => $access_point,
            "switch" => $switch,
            "keyboard" => $keyboard,
            "modem" => $modem,
            "cctv" => $cctv,
            "camera" => $camera,
            "printer" => $printer,
            "projector" => $projector,

            "female" => $female,
            "total_employee" => $total_employee,
            "male" => $male,
            "finance" => $finance,
            "it" => $it,
            "purchasing" => $purchasing,
            "pde" => $pde,
            "ppic" => $ppic,
            "hrd" => $hrd,
            "security" => $security,
            "maintanence" => $maintanence,
            "warehouse" => $warehouse,
            "finishing" => $finishing,
            "quality_control" => $quality_control,

            "total_email" => $total_email,
            "aktif" => "/overview",
            "items" => $item,   
        ];

        // dd($computer);

        return view('/overview/index.php', $data);
    }

    public function index2() 
    {
        $item = $this->itemModel_employee->orderBy("employee_id", "ASC")->paginate(5, "employee");

        $data = [
            "aktif" => "index2",
            "items" => $item,
        ];
        return view("/overview/index2", $data);
    }
}
