<?php

namespace App\Controllers;

use App\Models\ModelsLogin;
use \App\Models\Models;

class Signin extends BaseController
{
    protected $ModelLogin;

    public function __construct()
    {
        $this->ModelLogin = new ModelsLogin();
    }

    public function index()
    {
        $data = [
            "title" => "Sign In Page",
        ];
        return view("/login/index.php", $data);
    }

    public function login()
    {
        // $encrypter = \Config\Services::encrypter();

        // $users = $this->loginModel->where(["username" => $this->request->getVar("username")])->orWhere(["email" => $this->request->getVar("username")])->first();
        $users = $this->ModelLogin->where(["username" => $this->request->getVar("username")])->first();
        // dd($users);

        if (isset($users)) {
            // if (password_verify($this->request->getVar("password"), $users["password"])) {
            if ($this->request->getVar("password") === $users["password"]) {
                session()->set("username", $users["username"]);
                return redirect()->to("/asset/");
            } else {
                session()->setFlashdata("pesan_gagal", "username or password invalid");
                return redirect()->to("/signin/");
            }
        } else {
            session()->setFlashdata("pesan_gagal", "username or password invalid");
            return redirect()->to("/signin/");
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to("/signin/");
    }

    //--------------------------------------------------------------------

}
