<?php

namespace App\Controllers;

use \App\Models\ModelsAccess;

class Access extends BaseController
{
	protected $itemModel;

	public function __construct()
	{
		$this->itemModel = new ModelsAccess();
	}

	public function index()
	{
		if (!session()->get("username")) {
			return redirect()->to("/signin/");
		}

		$current_page = $this->request->getVar("page_access") ? $this->request->getVar("page_access") : 1;

		$keyword = $this->request->getVar("keyword");

		if ($keyword) {
			$item = $this->itemModel->search($keyword)->orderBy("id", "DESC")->paginate(20, "access");
		} else {
			$item = $this->itemModel->orderBy("id", "desc")->paginate(20, "access");
		};

		if (empty($item)) :
			$null = true;
		else :
			$null = false;
		endif;

		$pager = $this->itemModel->pager;

		$data = [
			"items" => $item,
			"aktif" => "/accessfunction/tambah",
			"keyword" => $this->request->getVar("keyword"),
			"pager" => $pager,
			"current_page" => $current_page,
			"validation" => \Config\Services::validation(),
			"title_insert" => "Insert View",
			"title_edit" => "Edit View",
			"null" => $null,
		];
		return view('/access/index.php', $data);
	}
}
