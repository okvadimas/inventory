-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2015 at 02:30 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `capstone`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `asset_id` varchar(10) NOT NULL,
  `asset_type` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_masuk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`asset_id`, `asset_type`, `asset_name`, `jumlah`, `supplier`, `keterangan`, `tgl_masuk`) VALUES
('A00001', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', 'Bumi Raya Indonesia', 'Original License', '06/18/2014'),
('A00002', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', 'Bumi Raya Indonesia', 'Original License', '07/17/2014'),
('A00003', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', 'Bumi Raya Indonesia', 'Original License', '08/18/2014'),
('A00004', 'OS', 'Windows 7 Professional SP1 32 Bit', '1', 'Bumi Raya Indonesia', 'Original License', '11/03/2015'),
('A00005', 'OS', 'Windows 7 Professional SP1 32 Bit', '2', 'Diamond Computer', 'Original License', '11/03/2015'),
('A00006', 'OS', 'Windows 8.1 Home Basic 64 bit', '1', 'Unknown', 'For Laptop PDE Manager ( Original License)', '11/03/2015'),
('A00007', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'BK Komputer', 'Original License', '03/14/2014'),
('A00008', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00009', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00010', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00011', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00012', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00013', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00014', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'Original License', '11/03/2015'),
('A00015', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', 'Unknown', 'For Laptop IT ( Original License)', '11/03/2015'),
('A00016', 'Computer', 'Komputer Rakitan', '1', 'Diamond Computer', 'For Employee', '11/03/2015'),
('A00017', 'Computer', 'Komputer Rakitan', '1', 'Diamond', 'For Employee', '11/03/2015'),
('A00018', 'Computer', 'Komputer Rakitan', '1', 'Diamond Computer', 'For Trainer', '11/03/2015'),
('A00019', 'Computer', 'Komputer Rakitan', '1', 'Maxcom', 'For Employee', '06/18/2014'),
('A00020', 'Printer', 'HP Laserjet 1025 CP', '1', 'Diamond Computer', 'For Employee', '11/03/2015'),
('A00021', 'Microsoft', 'Office 2016 Home & Business 2016 FPP', '7', 'Diamond Computer', 'Original License', '11/03/2015'),
('A00022', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00023', 'Laptop', 'Lenovo Vostro 1200', '1', 'Unknown', 'For Mam Mareciel', '11/03/2015'),
('A00024', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00025', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00026', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00027', 'Laptop', 'Fujitsu L H531', '1', 'Unknown', 'For Sir Tade', '11/03/2015'),
('A00028', 'Computer', 'All In One Lenovo C240', '1', 'BK Komputer', 'For Employee', '11/03/2015'),
('A00029', 'Laptop', 'Lenovo G410', '1', 'BK Komputer', 'For IT', '11/03/2015'),
('A00030', 'Laptop', 'HP 1000', '1', 'BK Komputer', 'For HRD Manager', '11/01/2014'),
('A00031', 'Computer', 'All In One Lenovo C340', '1', 'BK Komputer', 'For HRD Staff', '03/12/2014'),
('A00032', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00033', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For HRD', '11/03/2015'),
('A00034', 'Computer', 'Komputer Rakitan', '1', 'Bumi Raya Indonesia', 'For Packing', '11/03/2015'),
('A00035', 'Laptop', 'Lenovo Vostro 1200', '1', 'Unknown', 'For Sir Robert', '11/03/2015'),
('A00036', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00037', 'Computer', 'All In One Lenovo C240', '1', 'BK Komputer', 'For Employee', '11/03/2015'),
('A00038', 'Computer', 'All In One Lenovo C240', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00039', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00040', 'Laptop', 'HP 1431', '1', 'BK Komputer', 'For Manager Finishing', '11/03/2015'),
('A00041', 'Computer', 'Komputer Rakitan', '1', 'Bumi Raya Indonesia', 'For Manager Produksi', '08/18/2014'),
('A00042', 'Computer', 'All In One Lenovo C240', '1', 'Unknown', 'For Employee', '11/04/2015'),
('A00043', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For PDE', '11/03/2015'),
('A00044', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For PDE', '11/03/2015'),
('A00045', 'Computer', 'All In One Lenovo C240', '1', 'Unknown', 'For PDE', '11/03/2015'),
('A00046', 'Computer', 'HP Pavillion 20 All in One A2', '1', 'BK Komputer', 'For PDE', '03/12/2015'),
('A00047', 'Computer', 'All In One Lenovo C240', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00048', 'Laptop', 'Asus', '1', 'Nafiri Computer', 'For PDE Manager', '11/03/2015'),
('A00049', 'Laptop', 'Asus', '1', 'Unknown', 'For Costing', '11/03/2015'),
('A00050', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Warehouse', '11/03/2015'),
('A00051', 'Computer', 'Komputer Rakitan', '1', 'Unix,Indo Mandiri Computer', 'For Warehouse', '11/03/2015'),
('A00052', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00053', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '10/27/2015'),
('A00054', 'Computer', 'Dell Optiplex 7010 Builld Up', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00055', 'Computer', 'All In One Lenovo C240', '1', 'BK Komputer', 'For Employee', '11/03/2014'),
('A00056', 'Printer', 'HP Laserjet 1025 CP', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00057', 'Printer', 'HP Laserjet 1025 CP', '1', 'Unknown', 'For Employee', '11/03/2015'),
('A00058', 'Printer', 'HP Laserjet 1020', '1', 'Unknown', 'For HRD', '11/03/2015'),
('A00059', 'Printer', 'DocuPrint M205b Fuji Xerox', '1', 'Unknown', 'For PPIC', '11/03/2015'),
('A00060', 'Printer', 'Epson LX-310', '1', 'Bumi Raya Indonesia', 'For Employee', '10/02/2014'),
('A00061', 'Printer', 'Epson LX-310', '1', 'Sentra Komputer', 'For PPIC', '02/12/2014'),
('A00062', 'Printer', 'Epson LX-310', '1', 'Unknown', 'For Acounting', '11/03/2015'),
('A00063', 'Scanner', 'Canon Scan Lide 110', '1', 'Multicomp', 'For Purchasing', '04/15/2014'),
('A00064', 'Printer', 'DocuPrint M205b Fuji Xerox', '1', 'Unknown', 'For Acounting', '11/03/2015'),
('A00065', 'Server', 'Dell Power Edge 2950', '1', 'Cebu', 'For Server', '11/03/2015'),
('A00066', 'Router', 'Cisco 1800 Series', '1', 'Cebu', 'For Connection', '11/03/2015'),
('A00067', 'Switch', 'Catalyst 2960S series SI 24 Port', '1', 'Cebu', 'For Connection', '11/03/2015'),
('A00068', 'UPS', 'APC Smart-UPS 5000', '1', 'Cebu', 'For Server', '11/03/2015'),
('A00069', 'Modem', 'Linksys E 2500', '1', 'Cebu', 'For Wifi IT', '11/03/2015'),
('A00070', 'Modem', 'Linksys Cisco  E1200', '1', 'Cebu', 'For CCTV', '11/03/2015'),
('A00071', 'Modem', 'Linksys Cisco  E2500', '1', 'Cebu', 'For Wifi PTPF', '11/04/2015'),
('A00072', 'Server', 'Dell Power Edge R210', '1', 'Cebu', 'For Server', '11/03/2015'),
('A00073', 'CCTV', 'Calion 3024M', '1', 'Cebu', 'For CCTV', '11/03/2015'),
('A00074', 'Switch', 'HP 1405 8 Port V2', '1', 'Diamond Computer', 'For CCTV and Meeting Room ( Polycom)', '11/03/2015'),
('A00075', 'Projector', 'Epson EB 925', '1', 'Unknown', 'For Meeting', '11/03/2015'),
('A00076', 'Projector', 'Epson EB 925', '1', 'Unknown', 'For Meeting', '11/03/2015'),
('A00077', 'Printer', 'Barcode Printer Toshiba BEX4T', '1', 'Kahar Duta Sarana', 'Barcode Printer', '01/02/2014'),
('A00078', 'Hardware', 'HDD External Toshiba 1 TB canvio Basic 3.0', '1', 'Sentra Komputer', '1', '02/12/2014'),
('A00079', 'USB', 'USB 8 GB Toshiba', '1', 'Sentra Komputer', 'Flashdisk', '02/12/2014'),
('A00080', 'Hardware', 'Mesin Ketik Elektrik Brother GX-6750', '1', 'Sentra Komputer', 'Mesin Ketik', '02/19/2014'),
('A00081', 'UPS', 'UPS Prolink Pro 700S 650 watt', '4', 'Unix', 'UPS', '03/03/2014'),
('A00082', 'UPS', 'Battery UPS 8.2 A', '1', 'Unix', 'For UPS', '03/12/2014'),
('A00083', 'Laptop', 'Laptop LENOVO G400-2395', '1', 'BK Komputer', 'sir kent', '03/12/2014'),
('A00084', 'Hardware', 'Connector BNC', '2', 'Mitra Tehnik', 'For CCTV', '03/13/2014'),
('A00085', 'Hardware', 'Connector Jack DC', '2', 'Mitra Tehnik', 'For CCTV', '03/12/2014'),
('A00086', 'UPS', 'UPS Prolink Model = Pro 700V', '7', 'Multicomp', 'UPS For Computer', '03/19/2014'),
('A00087', 'Switch', 'Switch D-Link DGS-1008A', '1', 'Multicomp', 'For Internet', '04/15/2014'),
('A00088', 'UPS', 'UPS Prolink Type Pro 700', '6', 'Multicomp', 'UPS For Computer', '04/15/2014'),
('A00089', 'Cable', 'Kabel Lan Merk AMP Net Connect Category 6 Kualitasa Bagus', '2', 'Bina Utama electric', 'Per Roll', '03/17/2014'),
('A00090', 'Hardware', 'RJ 45 Kualitas Bagus Merk Bekden', '100', 'Bina Utama electric', 'For Internet', '06/08/2015'),
('A00091', 'UPS', 'UPS Prolink Pro 700 V', '1', 'Bumi Raya Indonesia', 'UPS For Computer', '06/18/2014'),
('A00092', 'CCTV', 'Camera, Model Cal-5120SHR', '1', 'Mitra Tehnik', 'For CCTV', '06/23/2014'),
('A00093', 'Hardware', 'Hardisk WDC Blue 3,5 '' Sn: WMCZE89779R', '1', 'Indo Mandiri Computer', 'Hardisk', '07/18/2014'),
('A00094', 'Camera', 'Kamera Digital Cannon Ixus 145 ( 16MP)', '1', 'Multicomp', 'Camera Digital', '08/08/2014'),
('A00095', 'Camera', 'Camera DIS 420', '1', 'Mitra Tehnik', 'For CCTV', '08/12/2014'),
('A00096', 'Laptop', 'Asus A450 LC', '1', 'Indo Mandiri Computer', 'For Costing', '08/12/2014'),
('A00097', 'Camera', 'Kamera Digital Merk : Fuji JX 680 16 MP', '1', 'Bursa Camera', '', '08/14/2014'),
('A00098', 'Camera', 'Kamera Merk Fuji JV - 500 ( 14 MP ) Bonus : Memory', '1', 'Bursa Camera', 'Camera', '08/15/2014'),
('A00099', 'UPS', 'UPS Prolink Pro 700 V', '1', 'Bumi Raya Indonesia', 'UPS For Computer', '08/18/2014'),
('A00100', 'Hardware', 'PSU 12 V 5 A and Box', '1', 'Mitra Tehnik', 'For CCTV', '09/16/2014'),
('A00101', 'CCTV', 'CCTV Calion', '5', 'Mitra Tehnik', 'For CCTV', '09/16/2014'),
('A00102', 'Switch', 'Switch hub hp Type : 1405, 8 part gigabyte', '100', 'Multicomp', 'For Internet', '09/22/2014'),
('A00103', 'LCD', 'LCD Projector Epson EB X 200', '1', 'Multicomp', 'LCD', '10/20/2014'),
('A00104', 'Router', 'Router cisco linksys E 2500', '1', 'Multicomp', 'For Internet', '11/06/2014'),
('A00105', 'Switch', 'Switch D - Link DÏƒS - 10008 A', '1', 'Indo Mandiri Computer', 'For Internet', '11/12/2014'),
('A00106', 'UPS', 'UPS prolink pro 700scf', '1', 'Bumi Raya Indonesia', 'UPS For Computer', '11/13/2014'),
('A00107', 'Mouse', 'Mouse usb logitech', '1', 'Angkasa', 'For Computer', '02/09/2015'),
('A00108', 'Keyboard', 'Keyboard usb logitech k 120', '1', 'Angkasa', 'For Computer', '02/09/2015'),
('A00109', 'UPS', 'UPS Prolink 650VA Pro 700 SFC', '2', 'Diamond Computer', 'UPS For Computer', '02/19/2015'),
('A00111', 'Switch', 'Switch D-Link DGS-1008A', '4', 'Diamond Computer', 'For Access Networking', '12/16/2015');

-- --------------------------------------------------------

--
-- Table structure for table `asset_pemakaian`
--

CREATE TABLE `asset_pemakaian` (
  `kode_asset` varchar(100) NOT NULL,
  `employee_id` varchar(100) NOT NULL,
  `asset_type` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `jumlah` varchar(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_keluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_pemakaian`
--

INSERT INTO `asset_pemakaian` (`kode_asset`, `employee_id`, `asset_type`, `asset_name`, `jumlah`, `keterangan`, `tgl_keluar`) VALUES
('1', 'Aniek Sulistyorini', 'Computer', 'All In One Lenovo C240', '1', '2', '11/10/2015'),
('2', 'Aniek Sulistyorini', 'OS', 'Windows 7 Home Basic SP1 32bit', '1', '2', '11/14/2015'),
('A82', 'Khoiri', 'Computer', 'Komputer Rakitan', '1', '1212', '11/30/2015');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `content_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`content_id`, `title`, `content`) VALUES
(2, 'Vision', '<pre><span style="font-size: large;"><strong>Vision</strong></span></pre>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: large;">&nbsp; Driven by its passion for continous improvement, the State College has to vigorously pursue distinction and proficieny in delivering its statutory functions to the Filipino people in the fields of education, business, agro-fishery, industrial, science and technology, through committed and competent human resource, guided by the beacon of innovation and productivity towards the heights of elevated status. </span><br /><br /></p>'),
(3, 'History', '<pre><span style="font-size: large;">HISTORY &nbsp;</span> </pre>\r\n<p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Carlos Hilado Memorial State College, formerly Paglaum State College, is a public educational institution that aims to provide higher technological, professional and vocational instruction and training in science, agriculture and industrial fields as well as short term or vocational courses. It was Batas Pambansa Bilang 477 which integrated these three institutions of learning: the Negros Occidental College of Arts and Trades (NOCAT) in the Municipality of Talisay, Bacolod City National Trade School (BCNTS) in Alijis, Bacolod City, and the Negros Occidental Provincial Community College (NOPCC) in Bacolod City, into a tertiary state educational institution to be called Paglaum State College. Approved in 1983, the College Charter was implemented effective January 1, 1984, with Mr. Sulpicio P. Cartera as its President. The administrative seat of the first state college in Negros Occidental is located at the Talisay Campus which was originally established as Negros Occidental School of Arts and Trades (NOSAT) under R.A. 848, authored and sponsored by Hon. Carlos Hilado. It occupies a five-hectare land donated by the provincial government under Provincial Board Resolution No. 1163. The renaming of the college to Carlos Hilado Memorial State College was effected by virtue of House Bill No. 7707 authored by then Congressman Jose Carlos V. Lacson of the 3rd Congressional District, Province of Negros Occidental, and which finally became a law on May 5, 1994</p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;">&nbsp;&nbsp;&nbsp; Talisay Campus. July 1, 1954 marked the formal opening of NOSAT with Mr. Francisco Apilado as its first Superintendent and Mr. Gil H. Tenefrancia as Principal. There were five (5) full time teachers, with an initial enrolment of eighty-nine (89) secondary and trade technical students. The shop courses were General Metal Works, Practical Electricity and Woodworking. The first classes were held temporarily at Talisay Elementary School while the shop buildings and classrooms were under construction. NOSAT was a recipient of FOA-PHILCUA aid in terms of technical books, equipment, tools and machinery. Alijis Campus. The Alijis Campus of the Carlos Hilado Memorial State College is situated in a 5-hectare lot located at Barangay Alijis, Bacolod City. The lot was a donation of the late Dr. Antonio Lizares. The school was formerly established as the Bacolod City National Trade School. The establishment of this trade technical institution is pursuant to R.A. 3886 in 1968, authored by the late Congressman Inocencio V. Ferrer of the second congressional district of the Province of Negros Occidental. Fortune Towne. The Fortune Towne Campus of the Carlos Hilado Memorial State College was originally situated in Negros Occidental High School (NOHS), Bacolod City on a lot owned by the Provincial Government under Provincial Board Resolution No. 91 series of 1970. The school was formerly established as the Negros Occidental Provincial Community College and formally opened on July 13, 1970 with the following course offerings: Bachelor of Arts, Technical Education and Bachelor of Commerce. The initial operation of the school started in July 13, 1970, with an initial enrolment of 209 students. Classes were first housed at the Negros Occidental High School while the first building was constructed. Then Governor Alfredo L. Montelibano spearheaded the first operation of the NOPCC along with the members of the Board of Trustees. In June 1995, the campus transferred to its new site in Fortune Towne, Bacolod City. Binalbagan Campus. On Nov. 24, 2000, the Negros Occidental School of Fisheries (NOSOF) in Binalbagan, Negros Occidental was integrated to the Carlos Hilado Memorial State College system as an external campus by virtue of Resolution No. 46 series of 2000.</p>'),
(4, 'Footer', '<p style="text-align:center">CHMSC Online Learning Managenment System</p>\r\n\r\n<p style="text-align:center">All Rights Reserved &reg;2013</p>\r\n'),
(5, 'Upcoming Events', '<pre>\r\nUP COMING EVENTS</pre>\r\n\r\n<p><strong>&gt;</strong> EXAM</p>\r\n\r\n<p><strong>&gt;</strong> INTERCAMPUS MEET</p>\r\n\r\n<p><strong>&gt;</strong> DEFENSE</p>\r\n\r\n<p><strong>&gt;</strong> ENROLLMENT</p>\r\n\r\n<p>&nbsp;</p>\r\n'),
(6, 'Title', '<p><span style="font-family:trebuchet ms,geneva">CHMSC Online Learning Management System</span></p>\r\n'),
(7, 'News', '<pre>\r\n<span style="font-size:medium"><em><strong>Recent News\r\n</strong></em></span></pre>\r\n\r\n<h2><span style="font-size:small">Extension and Community Services</span></h2>\r\n\r\n<p style="text-align:justify">This technology package was promoted by the College of Industrial Technology Unit is an index to offer Practical Skills and Livelihood Training Program particularly to the Ina ngTahanan of Tayabas, Barangay Zone 15, Talisay City, Negros Occidental</p>\r\n\r\n<p style="text-align:justify">The respondent of this technology package were mostly &ldquo;ina&rdquo; or mothers in PurokTayabas. There were twenty mothers who responded to the call of training and enhancing their sewing skills. The beginners projects include an apron, elastics waist skirts, pillow-cover and t-shirt style top. Short sleeve blouses with buttonholes or contoured seaming are also some of the many projects introduced to the mothers. Based on the interview conducted after the culmination activity, the projects done contributed as a means of earning to the respondents.</p>\r\n\r\n<p style="text-align:justify">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; In support to the thrust of the government to improve the health status of neighboring barangays, the Faculty and Staff of CHMSC ECS Fortune Towne, Bacolod City, launched its Medical Mission in Patag, Silay City. It was conducted last March 2010, to address the health needs of the people. A medical consultation is given to the residents of SitioPatag to attend to their health related problems that may need medical treatment. Medical tablets for headache, flu, fever, antibiotics and others were availed by the residents.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">&nbsp;The BAYAN-ANIHAN is a Food Production Program with a battle cry of &ldquo;GOODBYE GUTOM&rdquo;, advocating its legacy &ldquo;Food on the Table for every Filipino Family&rdquo; through backyard gardening. NGO&rsquo;s, governmental organizations, private and public sectors, business sectors are the cooperating agencies that support and facilitate this project and Carlos Hilado Memorial State College (CHMSC) is one of the identified partner school. Being a member institution in advocating its thrust, the school through its Extension and Community Services had conducted capability training workshop along this program identifying two deputy coordinators and trainers last November 26,27 and 28, 2009, with the end in view of implementing the project all throughout the neighboring towns, provinces and regions to help address poverty in the country. Program beneficiaries were the selected families of GawadKalinga (GK) in Hope Village, Brgy. Cabatangan, Talisay City, with 120 families beneficiaries; GK FIAT Village in Silay City with 30 beneficiaries; Bonbon Dream Village brgy. E. Lopez, Silay City with 60 beneficiaries; and respectively Had. Teresita and Had. Carmen in Talisay City, Negros Occidental both with 60 member beneficiaries. This program was introduced to 30 household members with the end in view of alleviating the quality standards of their living.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">The extension &amp; Community Services of the College conducted a series of consultations and meetings with the different local government units to assess technology needs to determines potential products to be developed considering the abundance of raw materials in their respective areas and their product marketability. The project was released in November 2009 in six cities in the province of Negros Occidental, namely San Carlos, Sagay, Silay, Bago, Himamaylan and Sipalay and the Municipality of E. B Magalona</p>\r\n\r\n<p style="text-align:justify">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The City of San Carlos focused on peanut and fish processing. Sagay and bago focused on meat processing, while Silay City on fish processing. The City of Himamaylan is on sardines, and in Sipalay focused on fish processing specially on their famous BARONGAY product. The municipality of E.B Magalona focused on bangus deboning.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The food technology instructors are tasked to provide the needed skills along with the TLDC Livelihood project that each City is embarking on while the local government units provide the training venue for the project and the training equipment and machine were provided by the Department of Science and Technology.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n'),
(8, 'Announcements', '<pre>\r\n<span style="font-size:medium"><em><strong>Announcements</strong></em></span></pre>\r\n\r\n<p>Examination Period: October 9-11, 2013</p>\r\n\r\n<p>Semestrial Break: October 12- November 3, 2013</p>\r\n\r\n<p>FASKFJASKFAFASFMFAS</p>\r\n\r\n<p>GASGA</p>\r\n'),
(10, 'Calendar', '<pre style="text-align:center">\r\n<span style="font-size:medium"><strong>&nbsp;CALENDAR OF EVENT</strong></span></pre>\r\n\r\n<table align="center" cellpadding="0" cellspacing="0" style="line-height:1.6em; margin-left:auto; margin-right:auto">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>First Semester &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>June 10, 2013 to October 11, 2013&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Semestral Break</p>\r\n			</td>\r\n			<td>\r\n			<p>Oct. 12, 2013 to November 3, 2013</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Second Semester</p>\r\n			</td>\r\n			<td>\r\n			<p>Nov. 5, 2013 to March 27, 2014</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Summer Break</p>\r\n			</td>\r\n			<td>\r\n			<p>March 27, 2014 to April 8, 2014</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Summer</p>\r\n			</td>\r\n			<td>\r\n			<p>April 8 , 2014 to May 24, 2014</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<table cellpadding="0" cellspacing="0" style="line-height:1.6em; margin-left:auto; margin-right:auto">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="4">\r\n			<p><strong>June 5, 2013 to October 11, 2013 &ndash; First Semester AY 2013-2014</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 4, 2013 &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Orientation with the Parents of the College&nbsp;Freshmen</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Service</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5</p>\r\n			</td>\r\n			<td>\r\n			<p>College Personnel General Assembly</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 6,7</p>\r\n			</td>\r\n			<td>\r\n			<p>In-Service Training (Departmental)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 10</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Classes</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Orientation with Students by College/Campus/Department</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 19,20,21</p>\r\n			</td>\r\n			<td>\r\n			<p>Branch/Campus Visit for Administrative / Academic/Accreditation/ Concerns</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan="2">\r\n			<p>June</p>\r\n			</td>\r\n			<td>\r\n			<p>Club Organizations (By Discipline/Programs)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Student Affiliation/Induction Programs</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July</p>\r\n			</td>\r\n			<td>\r\n			<p>Nutrition Month (Sponsor: Laboratory School)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July 11, 12</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August&nbsp; 8, 9</p>\r\n			</td>\r\n			<td>\r\n			<p>Midterm Examinations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 19</p>\r\n			</td>\r\n			<td>\r\n			<p>ArawngLahi</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 23</p>\r\n			</td>\r\n			<td>\r\n			<p>Submission of Grade Sheets for Midterm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August</p>\r\n			</td>\r\n			<td>\r\n			<p>Recognition Program (Dean&rsquo;s List)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 26</p>\r\n			</td>\r\n			<td>\r\n			<p>National Heroes Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August 28, 29, 30</p>\r\n			</td>\r\n			<td>\r\n			<p>Sports and Cultural Meet</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>September 19,20</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 5</p>\r\n			</td>\r\n			<td>\r\n			<p>Teachers&rsquo; Day / World Teachers&rsquo; Day</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 10, 11</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examination</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>October 12</p>\r\n			</td>\r\n			<td>\r\n			<p>Semestral Break</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<table cellpadding="0" cellspacing="0" style="margin-left:auto; margin-right:auto">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="4">\r\n			<p><strong>Nov. 4, 2013 to March 27, 2014 &ndash; Second Semester AY 2013-2014</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>November 4 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Start of Classes</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>November 19, 20, 21, 22</p>\r\n			</td>\r\n			<td>\r\n			<p>Intercampus Sports and Cultural Fest/College Week</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 5, 6</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 19,20</p>\r\n			</td>\r\n			<td>\r\n			<p>Thanksgiving Celebrations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 21</p>\r\n			</td>\r\n			<td>\r\n			<p>Start of Christmas Vacation</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 25</p>\r\n			</td>\r\n			<td>\r\n			<p>Christmas Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>December 30</p>\r\n			</td>\r\n			<td>\r\n			<p>Rizal Day (Regular Holiday)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 6, 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>Classes Resume</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 9, 10</p>\r\n			</td>\r\n			<td>\r\n			<p>Midterm Examinations</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>January 29</p>\r\n			</td>\r\n			<td>\r\n			<p>Submission of Grades Sheets for Midterm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>February 13, 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Long Tests</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 6, 7</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examinations (Graduating)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 13, 14</p>\r\n			</td>\r\n			<td>\r\n			<p>Final Examinations (Non-Graduating)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 17, 18, 19, 20, 21</p>\r\n			</td>\r\n			<td>\r\n			<p>Recognition / Graduation Rites</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>March 27</p>\r\n			</td>\r\n			<td>\r\n			<p>Last Day of Service for Faculty</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June 5, 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>First Day of Service for SY 2014-2015</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" style="margin-left:auto; margin-right:auto">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="2">\r\n			<p><strong>FLAG RAISING CEREMONY-TALISAY CAMPUS</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>MONTHS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>UNIT-IN-CHARGE</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>June, Sept. and Dec. 2013, March 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>COE</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>July and October 2013, Jan. 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>SAS</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>August and November 2013, Feb. 2014</p>\r\n\r\n			<p>April and May 2014</p>\r\n			</td>\r\n			<td>\r\n			<p>CIT</p>\r\n\r\n			<p>GASS</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n'),
(14, 'Campuses', '<pre>\r\n<span style="font-size:16px"><strong>Campuses</strong></span></pre>\r\n\r\n<ul>\r\n	<li>Alijis Campus</li>\r\n	<li>Binalbagan Campus</li>\r\n	<li>Fortunetown Campus</li>\r\n	<li>Talisay Campus<br />\r\n	&nbsp;</li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `domain`
--

CREATE TABLE `domain` (
  `domain_id` varchar(10) NOT NULL,
  `domain_user` varchar(50) NOT NULL,
  `domain_pass` varchar(50) NOT NULL,
  `domain_ip` varchar(50) NOT NULL,
  `employee_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `domain`
--

INSERT INTO `domain` (`domain_id`, `domain_user`, `domain_pass`, `domain_ip`, `employee_id`) VALUES
('D104', 'pdindriyasari', 'Ann1s4@30', '192.168.0.28', 'Putu Debbie Indriyasari'),
('D1053', 'esumantri', '3ri@2015', '192.168.0.64', 'Eri Sumantri'),
('D111', 'tarwanto', '@T@rw@nT0', '192.168.0.48', 'Tarwanto'),
('D115', 'sutanta', 'T@nt@2014', '192.168.0.66', 'Sutanta'),
('D1173', 'adinasari', '4yu@2015', '192.168.0.31', 'Ayu Dinasari'),
('D1186', 'rkurniasari', 'R@t1h2015', '192.168.0.49', 'Ratih Kurniasari'),
('D1207', 'akholidin', '@hm4d@2015', '192.168.0.22', 'Ahmad Kholidin'),
('D1211', 'mudhalifah', 'mudh@2015', '192.168.0.40', 'Mudhalifah'),
('D1212', 'dtriansah', 'd1k1@15', 'Wifi', 'Diki Triansah'),
('D123', 'haryono', '@76PsmTe', '192.168.0.57', 'Nawang Raharto'),
('D1231', 'wyuniati', 'w1nd@2015', 'not yet', 'Winda Yuniati'),
('D1246', 'bintoro', 'b1ntor0@15', '192.168.0.50', 'Bintoro Satriawan'),
('D1251', 'irahmawati', '1k@2015', '192.168.0.59', 'Ika Rahmawati'),
('D1252', 'ehartoyo', '3k0@2015', '192.168.0.42', 'Eko Hartoyo'),
('D1263', 'frochim', 'fauz@2015', '192.168.0.72', 'Fauzan Rochim'),
('D1265', 'hsuparyadi', 'h4rry@15', 'Wifi', 'Harry Suparyadi'),
('D15', 'tpmontes', 'T@d30@15', '192.168.0.56', 'Tadeo P Montes'),
('D16', 'mcperez', 'Mar3c13l', 'Wifi', 'Mareciel C Perez'),
('D185', 'sofian', 'S0f1@n15', '192.168.0.71', 'Kukuh Sediyatomo'),
('D319', 'vanny', 'v@nny@128', 'Wifi', 'Ivanny Vina Aviany'),
('D44', 'asulistyorini', 'An13k@15', '192.168.0.39', 'Aniek Sulistyorini'),
('D520', 'saminingsih', 'Diaz2403', '192.168.0.30', 'Sulistianah Aminingsih'),
('D540', 'masahlil', 'M@bdUl@15', '192.168.0.63', 'Yuni Umiarsih'),
('D566', 'dkrisna', 'D1@H2015', '192.168.0.29', 'Diah Krisnawati'),
('D672', 'dkurnianto', '74FF2h1k', '192.168.0.62', 'Nuryadi'),
('D737', 'msahlil', 's4hlil@15', '192.168.0.42', 'M. Abdul Sahlil'),
('D82', 'khoiri', 'KH0!r1@15', '192.168.0.31', 'Khoiri'),
('D850', 'esulistiono', '$ul1st10no', '192.168.0.52', 'Edy Sulistyono'),
('D852', 'splestari', '431@12R', '192.168.0.22', 'Sahebah Puji Lestari'),
('D853', 'kumam', 'K241@pf', '192.168.0.21', 'M. Khoirul Umam'),
('D952', 'pprabowo', 'PuT$@2015', '192.168.0.60', 'Putut Prabowo'),
('D962', 'antok', 'Ant0K@15', '192.168.0.61', 'Wagiyanto'),
('D994', 'msumaryono', 'Sum@Ry0no', '192.168.0.50', 'M. Sumaryono'),
('Dxx', 'irosita', '@r01msTa', '192.168.0.20', 'Sahebah Puji Lestari'),
('E1164', 'sanam', 'sayvirgo@09', '192.168.0.37', 'M. Syariful Anam');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `email_id` varchar(10) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `email_user` varchar(50) NOT NULL,
  `email_pass` varchar(50) NOT NULL,
  `email_ip` varchar(50) NOT NULL,
  `smtp` varchar(10) NOT NULL,
  `employee_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `email_address`, `email_user`, `email_pass`, `email_ip`, `smtp`, `employee_id`) VALUES
('E104', 'pdinriyasari@pacific-traders.com', 'pdinriyasari', 'P@971mse', '192.168.0.2', '25', 'Putu Debbie Indriyasari'),
('E1053', 'esumantri@pacific-traders.com', 'esumantri', '3ri@2015', '192.168.0.2', '25', 'Eri Sumantri'),
('E111', 'tarwanto@pacific-traders.com', 'tarwanto', 'T@w@3s0g', '192.168.0.2', '25', 'Tarwanto'),
('E115', 'sutanta@pacific-traders.com', 'sutanta', 'chE5et-f', '192.168.0.2', '25', 'Sutanta'),
('E1164', 'sanam@pacific-traders.com', 'sanam', 'An@m2015', '203.177.63.250/252', '26', 'M. Syariful Anam'),
('E1173', 'adinasari@pacific-traders.com', 'adinasari', 'ayu@2015', '192.168.0.2', '25', 'Ayu Dinasari'),
('E1186', 'rkurniasari', 'rkurniasari', 'rat1h@15', '192.168.0.2', '25', 'Ratih Kurniasari'),
('E1207', 'akholidin@pacific-traders.com', 'akholidin', 'ahm@d@15', '192.168.0.2', '25', 'Ahmad Kholidin'),
('E1212', 'dtriansah@pacific-traders.com', 'dtriansah', 'dik1@2015', '192.168.0.2', '25', 'Diki Triansah'),
('E123', 'nraharto@pacific-traders.com', 'nraharto', 'S7Educuy', '192.168.0.2', '25', 'Nawang Raharto'),
('E1231', 'wyuniati@pacific-traders.com', 'wyuniati', 'wind@2015', '192.168.0.2', '25', 'Winda Yuniati'),
('E1246', 'bsatriawan@pacific-traders.com', 'bsatriawan', 'bintor0@15', '192.168.0.2', '25', 'Bintoro Satriawan'),
('E1252', 'ehartoyo@pacific-traders.com', 'ehartoyo', 'ek0@2015', '192.168.0.2', '25', 'Eko Hartoyo'),
('E1263', 'frochim@pacific-traders.com', 'frochim', 'fauz@n15', '192.168.0.2', '25', 'Fauzan Rochim'),
('E1265', 'hsuparyadi@pacific-traders.com', 'hsuparyadi', 'harry@15', '203.177.63.250/252', '26', 'Harry Suparyadi'),
('E15', 'tpmontes@pacific-traders.com', 'tpmontes', 'T@d30@15', '124.83.56.178/9', '25/26', 'Tadeo P Montes'),
('E16', 'mcperez@pacific-traders.com ', 'mcperez', 'Mar3c13l', '124.83.56.178/9', '25/26', 'Mareciel C Perez'),
('E319', 'ivaviani@pacific-traders.com', 'ivaviani', '!2#4AbCd', '192.168.0.2', '25', 'Ivanny Vina Aviany'),
('E42', 'linhidayah@pacific-traders.com', '(el)linhidayah', 'm2th$Wap', '192.168.0.2', '25', 'Lukman Nurhidayah'),
('E44', 'asulistyorini@pacific-traders.com', 'asulistyorini', 'aniek@15', '203.177.63.250/252', '26', 'Aniek Sulistyorini'),
('E440', 'dwi@pacific-traders.com', 'dwi', '69DWi@13', '203.177.63.250/252', '26', 'Dwi Kurnianto'),
('E520', 'saminingsih@pacific-traders.com', 'saminingsih', 'Diaz2403', '192.168.0.2', '25', 'Sulistianah Aminingsih'),
('E540', 'yumiarsih@pacific-traders.com', 'yumiarsih', 'Yun1@14', '192.168.0.2', '25', 'Yuni Umiarsih'),
('E566', 'dkirsnawati@pacific-traders.com', 'dkrisnawati', 'D1ah@14', '192.168.0.2', '25', 'Diah Krisnawati'),
('E737', 'masahlil@pacific-traders.com', 'masahlil', 's5espaF*', '192.168.0.2', '25', 'M. Abdul Sahlil'),
('E82', 'khoiri@pacific-traders.com', 'khoiri', 'casw5pAC', '192.168.0.2', '25', 'Khoiri'),
('E850', 'esulistiyono@pacific-traders.com', 'esulistiono', 'Duf8a+us', '203.177.63.250/252', '26', 'Edy Sulistyono'),
('E852', 'splestari@pacific-traders.com', 'splestari', 'B4ugaSPa', '192.168.0.2', '25', 'Sahebah Puji Lestari'),
('E853', 'kumam@pacific-traders.com', 'kumam', 'V6x!jUtr', '192.168.0.2', '25', 'M. Khoirul Umam'),
('E952', 'pprabowo@pacific-traders.com', 'pprabowo', '4wUbR@ku', '192.168.0.2', '25', 'Putut Prabowo'),
('E962', 'antok@pacific-traders.com ', 'antok', '!2#4AbCd', '192.168.0.2', '25', 'Wagiyanto'),
('E994', 'msumaryono@pacific-traders.com', 'msumaryono', 'xEste8!V', '192.168.0.2', '25', 'M. Sumaryono'),
('HRD', 'hrdpacificfurniture@pacific-traders.com', 'hrdpacificfurniture', 'ptpacific14', '192.168.0.2', '25', '');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` varchar(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `brith_date` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `status` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `pake` varchar(30) NOT NULL,
  `file_sharing` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fullname`, `department`, `position`, `brith_date`, `no_hp`, `gender`, `status`, `address`, `pake`, `file_sharing`) VALUES
('000015', 'Tadeo P Montes', 'Outshourching', 'Manager', '10/27/1978', '082220370043', 'Male', 'Married', 'Cebu', 'Laptop', 'PPIC'),
('000016', 'Mareciel C Perez', '-', 'Operational Director', '06/15/1977', '081293465010', 'Female', '', 'Philippines', 'Laptop', 'All Dept'),
('000042', 'Lukman Nurhidayah', 'Divisi I', 'Manager', '08/14/1983', '', 'Male', '', 'JL. BRINGIN ASRI NO 139 R Ngaliyan Semarang', 'Computer', 'PPIC'),
('000044', 'Aniek Sulistyorini', 'HRD', 'Manager', '05/29/1972', '087825373688', 'Female', 'Married', 'Soreang Residance E2/6 Bandung', 'Laptop', 'HRD'),
('000073', 'Tri Mulyanto', 'PPIC', 'Supervisor Packing', '11/26/2015', '', 'Male', 'Married', 'Kaliwungu Semarang', 'Computer', 'None'),
('000082', 'Khoiri', 'Finance', 'Manager', '06/22/1978', '085641552080', 'Male', 'Single', 'Kedungsari RT 01/08 Semarang', 'Computer', 'Acounting, PPIC'),
('000104', 'Putu Debbie Indriyasari', 'Finance', 'Staff', '08/26/1978', '081326627177', 'Female', 'Married', 'Jl Padi IV B No.172 Semarang', 'Computer', 'Acounting , PPIC'),
('000111', 'Tarwanto', 'PPIC', 'Manager', '09/10/1980', '08587651866', 'Male', 'Married', 'Semarang', 'Computer', 'PPIC'),
('000115', 'Sutanta', 'Maintenance', 'Supervisor', '12/25/1978', '', 'Male', '', 'Jl Sriwibowo Rt. 03/06 Semarang', 'Computer', 'Maintenance'),
('000123', 'Nawang Raharto', 'PDE', 'Supervisor', '12/11/1977', '087731274408', 'Male', '', 'Dk. Talun Kacanf RT.04/III Kandri Gunung Pati Semarang', 'Computer', 'PDE'),
('000185', 'Kukuh Sediyatomo', 'Veneering', 'Leadman', '05/10/1978', '085290120139', 'Male', 'Married', 'Jl. Ronggowarsito AST Depo Indah 7A Semarang Timur', 'Computer', 'PPIC'),
('000319', 'Ivanny Vina Aviany', 'Purchasing', 'Staff', '01/23/1994', '087700108808', 'Female', '', 'Jl. Soekarno Hatta No.119A Semarang', 'Computer', 'Acounting, Maintenance, Purchasing'),
('000440', 'Dwi Kurnianto', 'Costing', 'Supervisor', '09/11/1976', '081325113869', 'Male', '', 'Jl.Brotojoyo Barat III / No.12 Semarang', 'Laptop', 'PDE Costing'),
('000520', 'Sulistianah Aminingsih', 'Finance', 'Staff', '08/26/1978', '081326307088', 'Female', '', 'Jl. Abu Bakar No.52 RT.15/12 Tambak Aji', 'Computer', 'Accounting'),
('000540', 'Yuni Umiarsih', 'Warehouse', 'Admin', '07/18/1980', '085740106048', 'Female', 'Married', 'Srikaton Utara IV Semarang', 'Computer', 'Warehouse'),
('000566', 'Diah Krisnawati', 'CSG', 'Supervisor', '06/24/1992', '082134718146', 'Female', 'Married', 'Genuk Sari Semarang', 'Computer', 'Secretary, PPIC'),
('000672', 'Nuryadi', 'PDE', 'Enginer', '05/16/1980', '085226083096', 'Male', '', 'Jl. Siwarak RW.02 Gunung Pati Semarang', 'Computer', 'PDE'),
('000737', 'M. Abdul Sahlil', 'HRD', 'Staff', '03/15/1995', '08988856203', 'Male', 'Single', 'Jl. Arimbi 1 Desa Galeh Purwodadi', 'Computer', 'HRD'),
('000819', 'Yuliharika Wati', 'Finishing', 'Admin', '07/21/1988', '081221999290', 'Male', 'Married', 'Semarang', 'Computer', 'PPIC'),
('000850', 'Edy Sulistyono', 'Finishing', 'Manager', '07/09/1980', '081215805522', 'Male', 'Married', 'Kaliwungu BIK B66 Kendal', 'Laptop', 'PPIC'),
('000852', 'Sahebah Puji Lestari', 'Purchasing', 'Staff', '07/04/1996', '', 'Female', '', 'Semarang', 'Computer', 'Purchasing'),
('000853', 'M. Khoirul Umam', 'Purchasing', 'Supervisor', '12/16/1994', '087747927822', 'Male', 'Single', 'Kendal', 'Computer', 'Purchasing, Secretary'),
('000952', 'Putut Prabowo', 'PDE', 'Staff', '03/27/1980', '08156595835', 'Male', '', 'Semarang', 'Computer', 'PDE'),
('000962', 'Wagiyanto', 'PDE', 'Drafter', '01/29/1979', '', 'Male', '', 'Jl. Kaliwiru II 509A Semarang', 'Computer', 'PDE'),
('000994', 'M. Sumaryono', 'PPIC', 'Staff', '08/23/1981', '085740683134', 'Male', 'Married', 'Gubug', 'Computer', 'PPIC'),
('001053', 'Eri Sumantri', 'Warehouse', 'Admin', '01/15/1992', '08999108676', 'Male', 'Single', 'Jl. Sendangguwo Raya Semarang', 'Computer', 'Warehouse'),
('001164', 'M. Syariful Anam', 'IT', 'IT', '09/07/1992', '087898118877', 'Male', 'Single', 'Perum Bukit Jatisari Lestari A22/5 RT.02/11 Mijen Semarang', 'Laptop', 'IT'),
('001173', 'Ayu Dinasari', 'Finance', 'Staff', '04/16/1992', '085641646920', 'Female', 'Married', 'Jl. Tugu Lapangan RT 04/1 Semarang', 'Computer', 'Acounting'),
('001186', 'Ratih Kurniasari', 'Maintenance', 'Admin', '07/22/1991', '089668200238', 'Female', 'Married', 'Mijen RT.03 RW II Semarang', 'Computer', 'Maintenance'),
('001205', 'Sudarmanto', 'Exim', 'Staff', '06/30/1987', '081329686351', 'Male', 'Single', 'Kendal', 'Computer', 'Exim'),
('001207', 'Ahmad Kholidin', 'Purchasing', 'Staff', '05/15/1994', '089668058604', 'Male', 'Single', 'Gubugsari 3/5 Pegandon Kendal', 'Computer', 'purchasing'),
('001211', 'Mudhalifah', 'HRD', 'Staff', '06/12/1992', '085741525724', 'Female', 'Single', 'Mangkang Wetan RT 03/02 Tugu Semarang', 'Computer', 'HRD'),
('001212', 'Diki Triansah', 'Outsourching', 'Staff', '12/01/1997', '083861745670', 'Male', 'Single', 'Putat Gede 02/06 Ngampel Kendal', 'Computer', 'PPIC'),
('001231', 'Winda Yuniati', 'Finance', 'Admin', '11/10/1994', '085879337078', 'Female', 'Single', 'Jl. Genuk Krajan 5 No 19 RT 06/03 Kel Tegalsari Kec Candisan Semarang', 'Computer', 'Acounting'),
('001246', 'Bintoro Satriawan', 'PPIC', 'Staff', '05/04/1987', '085641781160', 'Male', '', 'Semarang', 'Computer', 'PPIC, Acounting ( Debby)'),
('001251', 'Ika Rahmawati', 'Costing', 'Admin', '09/21/1994', '081391611041', 'Female', '', 'Tegorejo RT 1/8 Pegandon Kendal Semarang', 'Computer', 'PDE Costing'),
('001252', 'Eko Hartoyo', 'HRD', 'Supervisor', '07/18/1976', '08567794819', 'Male', '', 'Pondok Bukit Agung F-11 Semarang', 'Computer', 'HRD'),
('001263', 'Fauzan Rochim', 'Exim', 'Admin', '09/06/1992', '087832301254', 'Male', '', 'Jl. Dewi Sartuji Barat Semarang', 'Computer', 'CSG / Exim'),
('001265', 'Harry Suparyadi', 'HRD', 'Manager', '03/17/1977', '08122923096', 'Male', 'Married', 'Gedenganak Ungaran', 'Laptop', 'HRD');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(100) NOT NULL,
  `teacher_class_id` int(11) NOT NULL,
  `date_start` varchar(100) NOT NULL,
  `date_end` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `teacher_class_id`, `date_start`, `date_end`) VALUES
(15, 'Long Test', 113, '12/05/2013', '12/06/2013'),
(17, 'sdasf', 147, '11/16/2013', '11/16/2013');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`) VALUES
(15, 'admin', 'admin', 'admin', 'admin'),
(16, 'anam', 'anam', 'Syariful', 'Anam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`asset_id`);

--
-- Indexes for table `asset_pemakaian`
--
ALTER TABLE `asset_pemakaian`
  ADD PRIMARY KEY (`kode_asset`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `domain`
--
ALTER TABLE `domain`
  ADD PRIMARY KEY (`domain_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
